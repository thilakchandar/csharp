﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Print_triangle
{
    /// <summary>
    /// Main class of the program
    /// </summary>
    public class Class1
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the rows");
            int.TryParse(Console.ReadLine(), out int row);
            if (row <= 0 || row > 10)
            {
                Console.WriteLine("Value is not in range");
            }
            else
            {
                for (int i = 0; i < row; i++)
                {
                    //printing space so that * appears in middle
                    for (int j = 0; j < row - i; j++)
                    {
                        Console.Write(" ");
                    }
                    //* increases with increase in row
                    for (int k = 0; k <= i; k++)
                    {
                        Console.Write("* ");
                    }
                    Console.Write("\n");
                }
            }
        }
    }
}
