﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extract_bits
{
    /// <summary>
    /// Main program of class
    /// </summary>
    public class Class1
    {
        static void Main(string[] args)
        {
            int revStart = 0, endLength = 0;
            Console.WriteLine("Enter a number");
            int.TryParse(Console.ReadLine(),out int number);
            Console.WriteLine("Enter start bit");
            int.TryParse(Console.ReadLine(),out int start);
            Console.WriteLine("Enter end bit");
            int.TryParse(Console.ReadLine(),out int end);
            //converting to binary
            string binary = Convert.ToString(number, 2);
            int length = binary.Length;
            //converting the indexes in reverse position
            revStart = (length - end) - 1;
            endLength = (length - start) - revStart;
            //spliting the string
            string reqBinary = binary.Substring(revStart, endLength);
            int answer = Convert.ToInt32(reqBinary, 2);
            Console.WriteLine(answer);
        }
    }
}
