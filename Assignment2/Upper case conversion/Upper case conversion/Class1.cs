﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Upper_case_conversion
{
    /// <summary>
    /// Main class of the program
    /// </summary>
    public class Class1
    {
        static void Main(string[] args)
        {
            string words;
            while (true)
            {
                Console.WriteLine("Enter a string");
                //coverting to upper case
                words = Console.ReadLine().ToUpper();
                if (words == "STOP")
                {
                    break;
                }
                Console.WriteLine($"Case converted string is {words}");
            }
        }   
    }
}
