﻿using System;
namespace FindTheComplement 
{
    /// <summary>
    /// Main class of 
    /// </summary>
    public static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the number");
            int.TryParse(Console.ReadLine(), out int number);
            string binary = Convert.ToString(number, 2);
            string firstComp= " ";
            //converting 0 to 1 and 1 to 0
            foreach(char c in binary)
            {
                if (c == '0')
                {
                    firstComp = firstComp +"1";
                }
                else
                {
                    firstComp = firstComp +"0";
                }
            }
            firstComp = firstComp.Substring(1);
            int.TryParse(firstComp, out int firstCompliment);
            Console.WriteLine($"1's compliment:{firstCompliment}");
            //using xor for addition
            Console.WriteLine($"2's compliment:{firstCompliment ^ 1}");

        }
    }
}