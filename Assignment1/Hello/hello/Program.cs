﻿using System;
namespace Hello
{
    /// <summary>
    /// Main class for program 
    /// </summary>
    public class Program
    {
        static void Main(string[] args)
        {
            //printing hello world
            Console.WriteLine("Hello World");
        }
    }
}