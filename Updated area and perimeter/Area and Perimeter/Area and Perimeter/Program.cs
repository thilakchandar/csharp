﻿using System;
namespace Area_and_Perimeter
{
    public class Area_and_Perimeter
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Calculate the Area and Perimeter\n1.Square\n2.Rectangle\n3.Circle\n4.Triangle");
            int choice=Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    {
                        Console.WriteLine("Enter the side");
                        float side=float.Parse(Console.ReadLine());
                        Console.WriteLine($"Area:{side * side}\nPerimeter:{4 * side}");
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Enter length");
                        float length=float.Parse(Console.ReadLine());
                        Console.WriteLine("Enter breadth");
                        float breadth=float.Parse(Console.ReadLine());
                        Console.WriteLine($"Area:{length * breadth}\nPerimeter:{(2 * length) + (2 * breadth)}");
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Enter radius");
                        float radius=float.Parse(Console.ReadLine());
                        Console.WriteLine($"Area:{( radius * radius*22)/7}\nPerimeter:{(2*22*radius)/7}");
                        break;
                    }
                case 4:
                    {
                        Console.WriteLine("Enter height");
                        float height = float.Parse(Console.ReadLine());
                        Console.WriteLine("Enter base and side 1");
                        float side1 = float.Parse(Console.ReadLine());
                        Console.WriteLine("Enter side 2");
                        float side2 = float.Parse(Console.ReadLine());
                        Console.WriteLine("Enter side 3");
                        float side3 = float.Parse(Console.ReadLine());
                        Console.WriteLine($"Area:{0.5 * height * side1}\nPerimeter:{side1 + side2 + side3}");
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Invalid");
                        break;
                    }
            }
        }
    }
}