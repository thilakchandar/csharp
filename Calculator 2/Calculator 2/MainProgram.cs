﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator_2
{
    /// <summary>
    /// Main class of Calculator_and_its_logger
    /// </summary>
    public class MainProgram
    {
        public static List<LogList> logListCollection = new List<LogList>();
        static void Main(string[] args)
        {
            bool exit=false;
            while (true)
            {
                //Trying to catch exception
                try
                {
                    //int.TryParse(Console.ReadLine(), out int n);
                    var infix = new ArrayList();
                    double operand;
                    string operandString="";
                    bool closingBracket = false;
                    if (exit)
                    {
                        Thread T1 = new Thread(LogThread.Valid);
                        Thread T2 = new Thread(LogThread.Invalid);
                        T1.Start();
                        T2.Start();
                        return;
                    }
                    Console.WriteLine("Enter the expression to be calculated");
                    string? input = Console.ReadLine();
                    if (input==null) 
                    {
                        throw new NotImplementedException();
                    }
                    for(int i = 0; i < input.Length; i++)
                    {
                        if(input[i] =='+'|| input[i] == '-' || input[i] == '*' || input[i] == '/')
                        {
                            double.TryParse(operandString,out operand);
                            infix.Add(operand);
                            if (closingBracket)
                            {
                                infix.Add(')');
                                closingBracket = false;
                            }
                            infix.Add(input[i]);
                            operandString = "";
                        }
                        else if(input[i] == '(')
                        {
                            infix.Add(input[i]);
                        }
                        else if (input[i] == ')')
                        {
                            closingBracket = true;
                        }
                        else
                        {
                            operandString += input[i];
                        }
                    }
                    double.TryParse(operandString, out operand);
                    infix.Add(operand);
                    if(closingBracket)
                    {
                        infix.Add(')');
                    }
                    MainProgram main=new MainProgram();
                    Console.WriteLine(main.InfixToPostfix(infix));
                    Console.WriteLine("Press s to continue");
                    exit = true;
                    if(Console.ReadLine() == "s") { exit=false; }
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
        /// <summary>
        /// To convert to postfix to order the operators as per the priority
        /// </summary>
        /// <param name="infix">The expression which is passed</param>
        /// <returns>It returns the final value of the expression</returns>
        public double InfixToPostfix( ArrayList infix)
        {

            int priority = 0;
            var postfix = new ArrayList();
            Stack<object> stack = new Stack<object>();
            for (int i = 0; i < infix.Count; i++)
            {
                if (infix[i].Equals('-')|| infix[i].Equals('+') || infix[i].Equals('*') || infix[i].Equals('/') || infix[i].Equals('(') || infix[i].Equals(')'))
                {
                    if (stack.Count <= 0)
                        stack.Push(infix[i]);
                    else
                    {
                        if (infix[i].Equals('('))
                        {
                            stack.Push(infix[i]);
                        }
                        else if (infix[i].Equals(')'))
                        {
                            while ((char)stack.Peek() !='(')
                            {
                                postfix.Add(stack.Pop());
                            }
                            stack.Pop();
                        }
                        else
                        {
                            if ((char)stack.Peek() == '*' || (char)stack.Peek() == '/')
                                priority = 1;
                            else
                                priority = 0;
                            if (priority == 1)
                            {
                                if ((char)stack.Peek() == '(')
                                {
                                    stack.Push(infix[i]);
                                } 
                                postfix.Add(stack.Pop());
                                i--;
                            }
                            else
                            {
                                if (( (char)infix[i] == '+' || (char)infix[i] == '-')&&((char)stack.Peek()!= '(') )
                                {
                                    postfix.Add(stack.Pop());
                                    stack.Push(infix[i]);

                                }
                                else
                                    stack.Push(infix[i]);
                            }
                        }
                    }
                }
                else
                {
                    postfix.Add(infix[i]);
                }
            }
            int len = stack.Count;
            for (int j = 0; j < len; j++)
                postfix.Add(stack.Pop());
            return PostfixCalculation(postfix);
        }
        /// <summary>
        /// It evalutes the expression and returns the final answer 
        /// </summary>
        /// <param name="postfix">The expression in postfix</param>
        /// <returns>The value of the expression</returns>
        public double PostfixCalculation(ArrayList postfix)
        {
            Stack<double> stackCal = new Stack<double>();
            stackCal.Clear();
            for(int i = 0; i < postfix.Count; i++)
            {
                if (postfix[i].Equals('-') || postfix[i].Equals('+') || postfix[i].Equals('*') || postfix[i].Equals('/'))
                {
                    Operations<double> ops = new Operations<double>();
                    if (postfix[i].Equals('-'))
                    {
                        stackCal.Push(ops.Calculate((double)stackCal.Pop(),(double)stackCal.Pop(),"sub"));
                    }
                    else if (postfix[i].Equals('+'))
                    {
                        stackCal.Push(ops.Calculate((double)stackCal.Pop(), (double)stackCal.Pop(), "add"));
                    }
                    else if (postfix[i].Equals('*'))
                    {
                        stackCal.Push(ops.Calculate((double)stackCal.Pop(), (double)stackCal.Pop(), "mul"));
                    }
                    else if (postfix[i].Equals('/'))
                    {
                        stackCal.Push(ops.Calculate((double)stackCal.Pop(), (double)stackCal.Pop(), "div"));
                    }
                }
                else
                {
                    stackCal.Push((double)postfix[i]);
                }
            }
            return (double)stackCal.Pop();
        }
    }
}
