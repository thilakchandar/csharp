﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator_and_its_logger
{
    /// <summary>
    /// Converts value to string and updates to dictionary
    /// </summary>
    public class Updater
    {
        /// <summary>
        /// Updates log about the input,output and if it is successful 
        /// </summary>
        /// <param name="firstOperand">First operand</param>
        /// <param name="secondOperand">Second operand</param>
        /// <param name="output">Output</param>
        public static void log(double firstOperand,double secondOperand,double output)
        {
            string stringVal1= firstOperand.ToString();
            string stringVal2=secondOperand.ToString();
            string stringAns=output.ToString();
            LogList listObj=new LogList();
            listObj.valueToBeLogged = stringVal1 + ' ' + stringVal2 + ' ' + stringAns;
            listObj.success = true;
            MainProgram.logListCollection.Add(listObj);
        }
        /// <summary>
        /// Updates log
        /// </summary>
        /// <param name="firstOperand">First operand</param>
        /// <param name="secondOperand">Second operand</param>
        /// <param name="output">Output</param>
        public static void log(string firstOperand,string secondOperand,string output)
        {
            LogList listObj=new LogList();
            listObj.valueToBeLogged = firstOperand + ' ' + secondOperand + ' ' + output;
            listObj.success = true;
            MainProgram.logListCollection.Add(listObj);
        }
        /// <summary>
        /// Updates log for failed operations
        /// </summary>
        /// <param name="firstOperand">First operand</param>
        /// <param name="secondOperand">Second operand</param>
        public static void failedLog(dynamic firstOperand, dynamic secondOperand)
        {
            string stringVal1 = firstOperand.ToString();
            string stringVal2 = secondOperand.ToString();
            LogList listObj = new LogList(); 
            listObj.valueToBeLogged = stringVal1 + ' ' + stringVal2;
            listObj.success = false;
            MainProgram.logListCollection.Add(listObj);
        }

    }
}
