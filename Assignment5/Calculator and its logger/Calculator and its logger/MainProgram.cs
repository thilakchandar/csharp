﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator_and_its_logger
{
    /// <summary>
    /// Main class of Calculator_and_its_logger
    /// </summary>
    public class MainProgram
    {
        public static List<LogList> logListCollection = new List<LogList>();
        static void Main(string[] args)
        {
            while (true) {
                Console.WriteLine("1.Addition\n2.Subtraction\n3.Multiplication\n4.Division\n5.Exit");
                //Trying to catch exception
                try
                {
                    int.TryParse(Console.ReadLine(), out int n);
                    if (n == 5)
                    {
                        Thread T1 = new Thread(LogThread.Valid);
                        Thread T2 = new Thread(LogThread.Invalid);
                        T1.Start();
                        T2.Start();
                        return;
                    }
                    Console.WriteLine("Enter the two inputs");
                    var input1 =Console.ReadLine();
                    string? input2 = Console.ReadLine();
                    if(input1 == null || input2 == null) { return; }
                    //int dictSize = dict.Count;
                    if (n == 1)
                    {
                        Operations<dynamic>.Calculate(operand1: input1,
                                                      operand2: input2,
                                                      "add");
                    }
                    else if (n == 2)
                    {
                        Operations<dynamic>.Calculate(input1, input2,"sub");
                    }
                    else if (n == 3)
                    {
                        Operations<dynamic>.Calculate(input1, input2,"mul");
                    }
                    else if (n == 4)
                    {
                        Operations<dynamic>.Calculate(input1, input2,"div");
                    }
                    else
                    {
                        Console.WriteLine("This choice is invalid.\nTry again");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }     
        }
    }
}
