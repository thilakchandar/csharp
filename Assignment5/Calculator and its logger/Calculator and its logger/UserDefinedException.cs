﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator_and_its_logger
{
    /// <summary>
    /// User defined expression for string input
    /// </summary>
    public class StringException:Exception
    {
        /// <summary>
        /// Constructor for string input
        /// </summary>
        /// <param name="messeage">Message to be displayed</param>
        public StringException(string messeage):base(messeage)
        {
        }
    }
}
