﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator_and_its_logger
{
    /// <summary>
    /// Used to perform various operations
    /// </summary>
    public class PerformOperation
    {
        /// <summary>
        /// Used to perform addition operation 
        /// </summary>
        /// <param name="operand1">first operand</param>
        /// <param name="operand2">second operand</param>
        /// <returns>Sum of both the operand</returns>
        public double Add(double operand1,double operand2)
        {
            return operand1 + operand2;
        }
        /// <summary>
        /// Used to add strings
        /// </summary>
        /// <param name="operand1">first string</param>
        /// <param name="operand2">second string</param>
        /// <returns>Added string</returns>
        public string Add(string operand1,string operand2)
        {
            return operand1+ operand2;  
        }
        /// <summary>
        /// Used to perform subtraction
        /// </summary>
        /// <param name="operand1">first operand</param>
        /// <param name="operand2">second operand</param>
        /// <returns>Difference of both the operand</returns>
        public double Sub(double operand1,double operand2)
        {
            return operand1 - operand2;
        }
        /// <summary>
        /// Used to perform Multiplication
        /// </summary>
        /// <param name="operand1">first operand</param>
        /// <param name="operand2">second operand</param>
        /// <returns>Product of two operand</returns>
        public double Mul(double operand1, double operand2)
        {
            return operand1 * operand2;
        }
        /// <summary>
        /// Used to perform Division
        /// </summary>
        /// <param name="dividend">Dividend</param>
        /// <param name="divisor">Divisor</param>
        /// <returns>Quotient</returns>
        public double Div(double dividend, double divisor)
        {
            return dividend / divisor;
        }
    }
}
