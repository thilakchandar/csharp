﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator_and_its_logger
{
    internal class LogThread
    {
        /// <summary>
        /// To log valid operation
        /// </summary>
        public static void Valid()
        {
            var validOps = MainProgram.logListCollection.Where(i=>i.success==true).Select(i=>i.valueToBeLogged);
            StreamWriter writer = new StreamWriter("Valid.txt");
            foreach (string valueToBeLogged in validOps)
            {
                writer.WriteLine(valueToBeLogged);
            }
            writer.Close();
        }
        /// <summary>
        /// To log invalid operation
        /// </summary>
        public static void Invalid()
        {
            var invalidOps = MainProgram.logListCollection.Where(i=>i.success==false).Select(i=>i.valueToBeLogged);
            StreamWriter invalidWriter = new StreamWriter("Invalid.txt");
            foreach (string valueToBeLogged in invalidOps)
            {
                invalidWriter.WriteLine(valueToBeLogged);
            }
            invalidWriter.Close();
        }
    }
}
