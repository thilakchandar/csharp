﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator_and_its_logger
{
    /// <summary>
    /// Contains operations for which values should be passed
    /// </summary>
    /// <typeparam name="T">Generic type used so that any type can be passed</typeparam>
    public class Operations<T>
    {
        public Func<double, double, double> OperationDelegate;
        /// <summary>
        /// Initializing delegate for Operation
        /// </summary>
        /// <param name="operand1">First operand</param>
        /// <param name="operand2">Second operand</param>
        public delegate void Operation(T operand1, T operand2,string operation);
        
        /// <summary>
        /// Delegate for method calculation
        /// </summary>
        public static Operation Calculate=new Operation(Calculation);
        
        /// <summary>
        /// Used to perform calculation based on various operators
        /// </summary>
        /// <param name="firstOperand">First input entered</param>
        /// <param name="secondOperand">Second input entered</param>
        /// <exception cref="Exception">If any of the input is string</exception>
        public static void Calculation(T firstOperand, T secondOperand,string operation)
        {
            dynamic operand1 = firstOperand;
            dynamic operand2 = secondOperand;
            PerformOperation perform = new PerformOperation();
            Operations<T> operations = new Operations<T>();
            try
            {
                if (Regex1.StringTest(operand1, operand2) == false)
                {
                    operand1 = Convert.ToDouble(operand1);
                    operand2 = Convert.ToDouble(operand2);
                    double answer = 0;
                    if (operation == "add")
                    {
                        operations.OperationDelegate += perform.Add;
                        answer = operations.OperationDelegate.Invoke(operand1,operand2);
                        operations.OperationDelegate -=perform.Add;
                    }
                    else if (operation == "sub")
                    {
                        operations.OperationDelegate += perform.Sub;
                        answer = operations.OperationDelegate.Invoke(operand1, operand2);
                        operations.OperationDelegate -= perform.Sub;
                    }
                    else if (operation == "mul") 
                    {
                        operations.OperationDelegate += perform.Mul;
                        answer = operations.OperationDelegate.Invoke(operand1, operand2);
                        operations.OperationDelegate -= perform.Mul;
                    }
                    else if (operation == "div") 
                    {
                        operations.OperationDelegate += perform.Div;
                        answer = operations.OperationDelegate.Invoke(operand1, operand2);
                        operations.OperationDelegate -= perform.Div;
                    }
                    Console.WriteLine(answer);
                    Updater.log(operand1,operand2,answer);
                }
                else
                {
                    if (operation == "add")
                    {
                        string answer = perform.Add(operand1,operand2);
                        Console.WriteLine(answer);
                        Updater.log(operand1, operand2, answer);
                    }
                    else
                    {
                        throw new Exception("Invalid Input");
                    }
                }
                
            }
            catch (Exception e)
            {
                Updater.failedLog(operand1, operand2);
                Console.WriteLine(e.Message);
            }
        }
    }
}
