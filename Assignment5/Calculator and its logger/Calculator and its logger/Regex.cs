﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace Calculator_and_its_logger
{
    /// <summary>
    /// Use regex to check type
    /// </summary>
    internal class Regex1
    {
        /// <summary>
        /// Used to find the input is string and to update log
        /// </summary>
        /// <param name="firstInput">First input</param>
        /// <param name="secondInput">Second input</param>
        /// <returns></returns>
        public static bool StringTest(string firstInput, string secondInput)
        {
            Match match1 = Regex.Match(firstInput, @"\D");
            Match match2 = Regex.Match(secondInput, @"\D");
            return match1.Success||match2.Success;
        }


    }
}
