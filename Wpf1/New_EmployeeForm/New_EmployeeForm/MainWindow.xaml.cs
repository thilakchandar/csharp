﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using Microsoft.Win32;

namespace New_EmployeeForm
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Name_SelectionChanged(object sender, RoutedEventArgs e)
        {
            string name = this.Name.Text;
            Regex regex = new Regex(@"\d\W");
            Match match = regex.Match(name);
            if (string.IsNullOrEmpty(name)||match.Success)
            {
                this.Name.BorderBrush=Brushes.Red;
                this.IncorrectName.Text = "!InValid";
            }
            else
            {
                this.Name.BorderBrush=Brushes.White;
                this.IncorrectName.Text = "";
            }
        }

        private void Adress_SelectionChanged(object sender, RoutedEventArgs e)
        {
            string address = this.Address.Text;
            if (string.IsNullOrEmpty(address))
            {
                this.Address.BorderBrush = Brushes.Red;
                this.IncorrectAddress.Text = "!InValid";
            }
            else
            {
                this.Address.BorderBrush = Brushes.White;
                this.IncorrectAddress.Text = "";
            }
        }

        private void PhotoButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                Uri uri = new Uri(openFileDialog.FileName);
            }
        }

        private void PhoneNo_SelectionChanged(object sender, RoutedEventArgs e)
        {
            string phoneNo = this.PhoneNo.Text;
            Regex regex = new Regex(@"[0-9]{10}");
            Match match = regex.Match(phoneNo);
            if (match.Success)
            {
                this.PhoneNo.BorderBrush = Brushes.White;
                this.IncorrectPhoneNo.Text = "";               
            }
            else
            {
                this.PhoneNo.BorderBrush = Brushes.Red;
                this.IncorrectPhoneNo.Text = "!InValid";
            }
        }

        private void MailId_SelectionChanged(object sender, RoutedEventArgs e)
        {
            string mailId = this.MailId.Text;
            Regex regex = new Regex(@"\.com$");
            Regex regex1 = new Regex(@"@{1}"); 
            Match match = regex.Match(mailId);
            Match match1 = regex1.Match(mailId);
            if (match.Success && match1.Success)
            {
                this.MailId.BorderBrush = Brushes.White;
                this.IncorrectMailId.Text = "";
            }
            else
            {
                this.MailId.BorderBrush = Brushes.Red;
                this.IncorrectMailId.Text = "!InValid";
            }
        }

        private void ReportTo_SelectionChanged(object sender, RoutedEventArgs e)
        {
            string reportTo = this.ReportTo.Text;
            Regex regex = new Regex(@"\d\W");
            Match match = regex.Match(reportTo);
            if (string.IsNullOrEmpty(reportTo) || match.Success)
            {
                this.ReportTo.BorderBrush = Brushes.Red;
                this.IncorrectReportTo.Text = "!InValid";
            }
            else
            {
                this.ReportTo.BorderBrush = Brushes.White;
                this.IncorrectReportTo.Text = "";
            }
        }
    }
}
