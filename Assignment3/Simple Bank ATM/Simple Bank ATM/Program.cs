﻿using System;
namespace Simple_Bank_ATM
{
    /// <summary>
    /// Main class of program
    /// </summary>
    public class Program
    {
        /// <summary>
        /// enum for operations
        /// </summary>
        public enum operations
        {
            Withdrawal='a',
            Deposit='b',
            MiniStatement='c',
            NameChange='d',
            Exit='e'
        }
        /// <summary>
        /// Structure of accounts
        /// </summary>
        public struct accounts
        {
            public string name;
            public short pin;
            public float balance;
            /// <summary>
            /// Withdrawing operations
            /// </summary>
            /// <param name="Amount">Amount to be withdrawed</param>
            public void Withdrawal(float Amount)
            {
                balance -= Amount;
                Console.WriteLine("Remaining balance:{0}", balance);
            }
            /// <summary>
            /// Depositing operation
            /// </summary>
            /// <param name="Amount">Amount to be deposited</param>
            public void Deposit(float Amount)
            {
                balance += Amount;
            }
        }
        static void Main(string[] args)
        {
            accounts[] accountsArray = new accounts[2];
            //hardcoding values
            accountsArray[0].name = "Thilak";
            accountsArray[0].pin = 0000;
            accountsArray[0].balance = 5550;
            accountsArray[1].name = "Vinay";
            accountsArray[1].pin = 1234;
            accountsArray[1].balance = 3000;
            int flag = 0,j=0,pinValidation=0;
            while (true)
            {
                Console.WriteLine("Choose a,b,c,d,e\na.Withdrawal\nb.Deposit\nc.Mini statement\nd.Name change\ne.exit");
                char.TryParse(Console.ReadLine(),out char operation);
                operations current = (operations)operation ;
                if ( current == operations.Exit)
                {
                    return;
                }
                if (pinValidation == 0)
                {
                    Console.WriteLine("Enter pin");
                    int.TryParse(Console.ReadLine(),out int pin);
                    for (j = 0; j < accountsArray.Length; j++)
                    {
                        if (accountsArray[j].pin == pin)
                        {
                            flag = 1; pinValidation = 1; break;
                        }
                    }
                    if (flag == 0)
                    {
                        Console.WriteLine("Invalid Pin");
                        continue;
                    }
                }
                switch(current)
                {
                    case operations.Withdrawal:
                    {
                            Console.WriteLine("Enter amount");
                            float.TryParse(Console.ReadLine(),out float amount);
                            if (accountsArray[j].balance-amount >= 500)
                            {
                                accountsArray[j].Withdrawal(amount);
                            }
                            else
                            {
                                Console.WriteLine("Minimum balance should always be >= 500");
                            }
                            break;
                    }
                    case operations.Deposit:
                        {
                            Console.WriteLine("Enter amount");
                            float.TryParse(Console.ReadLine(),out float amount);
                            if (amount > 10000)
                            {
                                Console.WriteLine("Maximun deposit must be 10000");
                            }
                            else if (amount + accountsArray[j].balance > 500000)
                            {
                                Console.WriteLine("Maximum balance can be 500000");
                            }
                            else
                            {
                                accountsArray[j].Deposit(amount);
                                Console.WriteLine("Current balance : {0}", accountsArray[j].balance);
                            }
                            break;
                        }

                    case operations.MiniStatement:
                        {
                            Console.WriteLine("Balance : {0}", accountsArray[j].balance);
                            break;
                        }
                    case operations.NameChange:
                        {
                            Console.WriteLine("Enter new name");
                            string newName=Console.ReadLine();
                            Console.WriteLine($"Previous name : {accountsArray[j].name}");
                            accountsArray[j].name = newName;
                            Console.WriteLine($"Name is updated as {accountsArray[j].name}");
                            break;
                        }
                
                }
            }
            
            

        }
    }
}