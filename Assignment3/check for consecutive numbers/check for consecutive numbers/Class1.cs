﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace check_for_consecutive_numbers
{
    /// <summary>
    /// Main class for program
    /// </summary>
    public class Class1
    {
            static void Main(string[] args)
            {
                Console.WriteLine("Enter string");
                string input = Console.ReadLine();
                int length = input.Length;
                int[] array = new int[length / 2 + 1];
                int i = 0;
                //splitting input to numbers and assigning it to array
                foreach (char c in input)
                {
                    if (c != '-')
                    {
                        array[i++] = (int)c;
                    }
                }
                //if numbers are in desecending order
                if (array[0] - array[1] == 1)
                {
                    for (i = 0; i < array.Length - 1; i++)
                    {
                        if (array[i] - array[i + 1] != 1)
                        {
                            Console.WriteLine("Non consecutive number");
                            return;
                        }
                    }
                }
                //if numbers are in ascending order
                else if (array[1] - array[0] == 1)
                {
                    for (i = 0; i < array.Length - 1; i++)
                    {
                        if (array[i] - array[i + 1] != -1)
                        {
                            Console.WriteLine("Non consecutive number");
                            return;
                        }
                    }

                }
                else
                {
                    Console.WriteLine("Non consecutive number");
                    return;
                }
                Console.WriteLine("Consecutive number");
            }
        
    }
}
