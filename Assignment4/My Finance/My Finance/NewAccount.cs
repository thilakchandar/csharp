﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My_Finance
{
    /// <summary>
    /// Class to create new account
    /// </summary>
    public class NewAccount
    {
        /// <summary>
        /// Constructor to create new account
        /// </summary>
        /// <param name="choice1">To choose if its a savings or fd or loan</param>
        public NewAccount(int choice1)
        {
            switch (choice1)
            {
                case 1:
                    {
                        SavingsAccount savingsAccount1 = new SavingsAccount();
                        Console.WriteLine("Enter the amount to be deposited");
                        double.TryParse(Console.ReadLine(), out double result);
                        savingsAccount1.Deposit(result);
                        //Adding savings account to the array
                        Program.SavingAccounts[Program.noOfSavings] = savingsAccount1;
                        Program.noOfSavings++;
                        break;
                    }
                case 2:
                    {
                        FdAccount fdAccount1 = new FdAccount();
                        Console.WriteLine("Enter the amount to be deposited");
                        double.TryParse(Console.ReadLine(), out double result);
                        fdAccount1.Deposit(result);
                        Console.WriteLine("Enter the maturity period");
                        int.TryParse(Console.ReadLine(), out int resultfd);
                        fdAccount1.Maturity = resultfd;
                        Console.WriteLine("Choose bank\n1.SBI\n2.ICICI\n3.HDFC");
                        int.TryParse(Console.ReadLine(), out int bankChoice);
                        //Assigning interest based on bank and maturity
                        if (fdAccount1.Maturity < 2)
                        {
                            fdAccount1.Interest = Program.banks[bankChoice - 1].BelowTwo;
                        }
                        else if (fdAccount1.Maturity <= 7)
                        {
                            fdAccount1.Interest = Program.banks[bankChoice - 1].TwoToSeven;
                        }
                        else if (fdAccount1.Maturity < 10)
                        {
                            Console.WriteLine("No FD avilable");
                        }
                        else
                        {
                            fdAccount1.Interest = Program.banks[bankChoice - 1].TenAbove;
                        }
                        Program.FdAccounts[Program.noOfFd] = fdAccount1;
                        Program.noOfFd++;
                        break;

                    }
                case 3:
                    {
                        LoanAccount loanAccount1 = new LoanAccount();
                        Console.WriteLine("Enter the amount to be withdrawed");
                        double.TryParse(Console.ReadLine(), out double result);
                        loanAccount1.Withdraw(result);
                        Console.WriteLine("Enter the maturity period");
                        int.TryParse(Console.ReadLine(), out int maturity);
                        loanAccount1.Maturity = maturity;
                        Console.WriteLine("Choose bank\n1.SBI\n2.ICICI\n3.HDFC");
                        int.TryParse(Console.ReadLine(), out int bankChoice);
                        //Assigning interest based on bank
                        loanAccount1.Interest = Program.banks[bankChoice - 1].Loan;
                        Program.LoanAccounts[Program.noOfLoan] = loanAccount1;
                        Program.noOfLoan++;
                        break;
                    }


            }
        }
    }
}
