﻿using System;
namespace My_Finance
{
    /// <summary>
    /// Main class of program
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Creating array for savings account
        /// </summary>
        public static SavingsAccount[] SavingAccounts = new SavingsAccount[5];
        /// <summary>
        /// Creating array for fda ccount
        /// </summary>
        public static FdAccount[] FdAccounts = new FdAccount[5];
        /// <summary>
        /// Creating array for loan account
        /// </summary>
        public static LoanAccount[] LoanAccounts = new LoanAccount[5];
        public static Bank[] banks=new Bank[3];
        /// <summary>
        /// Contains number of Saving accounts
        /// </summary>
        public static int noOfSavings = 0;
        /// <summary>
        /// Contains number of FdAccounts
        /// </summary>
        public static int noOfFd = 0;
        /// <summary>
        /// Contains number of Loan accounts
        /// </summary>
        public static int noOfLoan = 0;
        /// <summary>
        /// Contains the choice
        /// </summary>
        public static int choice = 0;
        static void Main(string[] args)
        {
            //Updating interest rates
            Bank sbi = new Bank();
            Bank icici = new Bank();
            Bank hdfc = new Bank();
            sbi.BelowTwo = (float)4.4;
            sbi.TwoToSeven = (float)5;
            sbi.TenAbove = (float)5.5;
            sbi.Loan = (int)15;
            icici.BelowTwo = (float)4.7;
            icici.TwoToSeven = (float)5.2;
            icici.TenAbove = (float)5.5;
            icici.Loan = (int)17;
            hdfc.BelowTwo = (float)4.4;
            hdfc.TwoToSeven = (float)5;
            hdfc.TenAbove = (float)5.6;
            hdfc.Loan = (int)20;
            Program.banks[0] = sbi;
            Program.banks[1] = icici;
            Program.banks[2] = hdfc;
            
            try {
                //Dispaying the first set of menus
                while (true)
                {
                    Console.WriteLine("1. Open a account");
                    for (int j = 1; j <= noOfSavings; j++)
                    {
                        Console.WriteLine($"{j + 1}. Savings Account {j}");
                    }
                    Console.WriteLine($"{noOfSavings + 2}. See Account Summary");
                    int.TryParse(Console.ReadLine(), out choice);
                    if (choice == 1)

                    {
                        Console.WriteLine("Choose an account type \n1.Savings\n2.FD\n3.Loan");
                        int.TryParse(Console.ReadLine(),out int choice1);
                        //Choosing the type of account
                        NewAccount a = new NewAccount(choice1);

                    }

                    else if (choice < noOfSavings + 2)
                    {
                        Console.WriteLine("a.View Balance\nb.Deposit\nc.Withdraw\nd.Main Menu");
                        char.TryParse(Console.ReadLine(), out char result);
                        ViewSavings viewing=new ViewSavings(result);
                    }
                    else if (choice == noOfSavings + 2) {
                        NetWorth net=new NetWorth();
                        return;
                    }


                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            }
    }
}