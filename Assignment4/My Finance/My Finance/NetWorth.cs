﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My_Finance
{
    /// <summary>
    /// Class for returning networth
    /// </summary>
    public class NetWorth
    {
        /// <summary>
        /// Constructor for returning networth
        /// </summary>
        public NetWorth()
        {
            int i = 0;
            double netWorth = 0;
            for (int j = 0; j < Program.noOfSavings; j++)
            {
                netWorth += Program.SavingAccounts[j].Balance;
            }
            for (int j = 0; j < Program.noOfFd; j++)
            {
                netWorth += Program.FdAccounts[j].Balance;
                netWorth += Program.FdAccounts[j].Profit();
            }
            for (int j = 0; j < Program.noOfLoan; j++)
            {
                netWorth += Program.LoanAccounts[j].Balance;
                netWorth += Program.LoanAccounts[j].Liability();
            }
            Console.WriteLine($"Net Worth: {netWorth}");
            for (int j = 0; j < Program.noOfSavings; j++)
            {
                i++;
                Console.WriteLine($"Saving account{i} balance: {Program.SavingAccounts[j].Balance}");
            }
            i = 0;
            for (int j = 0; j < Program.noOfFd; j++)
            {
                i++;
                Console.WriteLine($"Fd account{i} balance: {Program.FdAccounts[j].Balance}");
                Console.WriteLine($"Fd account{i} profit: {Program.FdAccounts[j].Profit()}");
            }
            i = 0;
            for (int j = 0; j < Program.noOfLoan; j++)
            {
                i++;
                Console.WriteLine($"Loan account{i} balance: {Program.LoanAccounts[j].Balance}");
                Console.WriteLine($"Loan account{i} liability: {Program.LoanAccounts[j].Liability()}");
            }
        }
    }
}
