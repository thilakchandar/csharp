﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My_Finance
{
    /// <summary>
    /// class for basic class
    /// </summary>
    public class Account
    {
        private double balance = 0;
        /// <summary>
        /// Contains balance
        /// </summary>
        public double Balance { get { return balance; } set { balance = value; } }
        /// <summary>
        /// Logic for deposisting
        /// </summary>
        /// <param name="amount">the amount to be deposisted</param>
        public void Deposit(double amount)
        {
            balance += amount;
            Console.WriteLine($"The balance is {balance}");
        }
    }
}
