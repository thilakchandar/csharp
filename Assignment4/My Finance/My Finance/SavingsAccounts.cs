﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My_Finance
{

    /// <summary>
    /// Class for savings account
    /// </summary>
    public class SavingsAccount : Account
    {
        /// <summary>
        /// Logic for withdrawing
        /// </summary>
        /// <param name="amount">amount to be withdrawed</param>
        public void Withdraw(double amount)
        {
            Balance -= amount;
            Console.WriteLine($"The current balance is {Balance}");
        }
    }
}
