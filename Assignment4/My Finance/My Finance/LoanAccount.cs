﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My_Finance
{
    /// <summary>
    /// Class for loan account
    /// </summary>
    public class LoanAccount
    {
        private int maturity;
        /// <summary>
        /// holds the maturity value
        /// </summary>
        public int Maturity { get { return maturity; } set { maturity = value; } }
        private double balance = 0;
        /// <summary>
        /// holds the balance for loan account
        /// </summary>
        public double Balance { get { return balance; } set { balance = value; } }
        /// <summary>
        /// Logic for withdrawed
        /// </summary>
        /// <param name="amount">amount to be withdrawed</param>
        public void Withdraw(double amount)
        {
            balance -= amount;
        }
        private float interest;
        /// <summary>
        /// holds the value for interest
        /// </summary>
        public float Interest { get { return interest; } set { interest = value; } }
        /// <summary>
        /// Calculates liability
        /// </summary>
        /// <returns>liablity</returns>
        public double Liability()
        {
            return (balance * interest * maturity) / 100;
        }
    }
}