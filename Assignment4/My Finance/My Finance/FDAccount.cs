﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My_Finance
{
    /// <summary>
    /// Class for fixed deposit
    /// </summary>
    public class FdAccount : Account

    {
        private int maturity;
        /// <summary>
        /// holds maturity details
        /// </summary>
        public int Maturity { get { return maturity; } set { maturity = value; } }
        private float interest;
        /// <summary>
        /// holds interest details
        /// </summary>
        public float Interest { get { return interest; } set { interest = value; } }
        /// <summary>
        /// calculates profit
        /// </summary>
        /// <returns>profit</returns>
        public double Profit()
        {
            return (Balance * interest * maturity) / 100;
        }
    }
}
