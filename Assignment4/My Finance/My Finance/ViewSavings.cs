﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My_Finance
{
    /// <summary>
    /// Class for accessing savings account
    /// </summary>
    public class ViewSavings
    {
        /// <summary>
        /// Constructor for accessing savings account 
        /// </summary>
        /// <param name="result">To choose between various actions in savings account</param>
        public ViewSavings(char result)
        {
            switch (result)
            {
                case 'a':
                    {
                        Console.WriteLine($"Balance is : {Program.SavingAccounts[Program.choice - 2].Balance}");
                        break;
                    }
                case 'b':
                    {
                        Console.WriteLine("Amount to be deposited");
                        double.TryParse(Console.ReadLine(), out double depositAmount);
                        Program.SavingAccounts[Program.choice - 2].Deposit(depositAmount);
                        break;
                    }
                case 'c':
                    {
                        Console.WriteLine("Amount to be withdrawed");
                        double.TryParse(Console.ReadLine(), out double amountWithdraw);
                        if (amountWithdraw > Program.SavingAccounts[Program.choice - 2].Balance)
                        {
                            Console.WriteLine("Not enough balance");
                        }
                        else
                        {
                            Program.SavingAccounts[Program.choice - 2].Withdraw(amountWithdraw);
                            Console.WriteLine($"Current balance is {Program.SavingAccounts[Program.choice - 2].Balance}");
                        }
                        break;
                    }
                case 'd':
                    {
                        break;
                    }

            }
        }
    }
}
