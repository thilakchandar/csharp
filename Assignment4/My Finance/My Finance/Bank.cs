﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My_Finance
{
    /// <summary>
    /// Class for holding bank details
    /// </summary>
    public class Bank
    {
        private float belowTwo;
        /// <summary>
        /// holds interest details for less than 2 years
        /// </summary>
        public float BelowTwo { get { return belowTwo; } set { belowTwo = value; } }
        private float twoToSeven;
        /// <summary>
        /// holds interest details for 2 to 7 years
        /// </summary>
        public float TwoToSeven { get { return twoToSeven; } set { twoToSeven = value; } }
        private float tenAbove;
        /// <summary>
        /// holds interest details for above 10 years
        /// </summary>
        public float TenAbove { get { return tenAbove; } set { tenAbove = value; } }
        private int loan;
        /// <summary>
        /// holds details for loan interest
        /// </summary>
        public int Loan { get { return loan; } set { loan = value; } }
    }
}
