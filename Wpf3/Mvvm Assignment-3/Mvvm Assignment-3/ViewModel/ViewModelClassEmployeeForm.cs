﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mvvm_Assignment_3.Model;
using Mvvm_Assignment_3;
using System.ComponentModel;
using System.Windows.Media;
using System.Text.RegularExpressions;
using System.Windows;
using Microsoft.Win32;
using System.Windows.Media.Imaging;

namespace Mvvm_Assignment_3.ViewModel
{
    public class ViewModelClassEmployeeForm : INotifyPropertyChanged,IDataErrorInfo
    {
        ModelClassEmployeeForm employee = new ModelClassEmployeeForm();
        /// <summary>
        /// Command to open file folder to choose image
        /// </summary>
        public RelayCommand imageButton { get; set; }
        /// <summary>
        /// Command to close the employeeform window
        /// </summary>
        public RelayCommand cancel { get; set; }
        /// <summary>
        /// Command to add employee details to listview
        /// </summary>
        public RelayCommand add { get; set; }
        /// <summary>
        /// Command to get value from checkbox
        /// </summary>
        public RelayCommand checkBox { get; set; }
        /// <summary>
        /// Command to get value from radiobutton 
        /// </summary>
        public RelayCommand radioButton { get; set; }
        /// <summary>
        /// Constructor
        /// </summary>
        public ViewModelClassEmployeeForm()
        {
            imageButton = new RelayCommand(ChooseImage);
            cancel = new RelayCommand(employee.CloseWindow);
            add = new RelayCommand(employee.AddToList,isValid);
            checkBox = new RelayCommand(Married);
            radioButton = new RelayCommand(GenderChoose);
        }
        /// <summary>
        /// Name of employee
        /// </summary>
        public string Name
        {
            set
            {
                employee.Name = value;
            }
            get
            {
                return employee.Name;
            }
        }      
        /// <summary>
        /// Employee's address
        /// </summary>
        public string Address
        {
            set
            {
                employee.Address = value;
            }
            get
            {
                return employee.Address;
            }
        }
        /// <summary>
        /// Image of employee
        /// </summary>
        public void ChooseImage(object parameter)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.jpg,*bmp)|*.jpg";
            if (openFileDialog.ShowDialog() == true)
            {
                employee.Image = new BitmapImage(new Uri(openFileDialog.FileName));
            }
        }
        /// <summary>
        /// Method to be put in to command radioButton
        /// </summary>
        /// <param name="parameter">Contains the content of the choosen radio button</param>
        public void GenderChoose(object parameter)
        {
            Gender = (string)parameter;
        }
        /// <summary>
        /// Employee's gender
        /// </summary>
        public string Gender
        {
            set
            {
                employee.Gender= value;
            } 
            get 
            { 
                return employee.Gender; 
            }
        }
        /// <summary>
        /// Method to be put into checkbox
        /// </summary>
        /// <param name="parameter">Contains the boolean value which denotes if the checkbox is checked or not</param>
        public void Married(object parameter)
        {
            if ((bool)parameter)
            {
                IsMarried = "Married";
            }
            else
            {
                IsMarried = "Single";
            }
        }
        /// <summary>
        /// Employee's martial status
        /// </summary>
        public string IsMarried
        {
            set
            {
                employee.IsMarried = value;
            }
            get 
            { 
                return employee.IsMarried;
            }
        }
        /// <summary>
        /// Employee's Phone number
        /// </summary>
        public string PhoneNo
        {
            set
            {
                employee.PhoneNo = value;
            }
            get
            {
                return employee?.PhoneNo;
            }
        }
        /// <summary>
        /// Mailid of employee
        /// </summary>
        public string MailId
        {
            set
            {
                employee.MailId = value;
            }
            get
            {
                return employee.MailId;
            }
        }
        /// <summary>
        /// Position of employee
        /// </summary>
        public string Position
        {
            set
            {
                employee.Position = value;
                if (employee.Position == "Project Enginner")
                {
                    employee.Salary = 40000;
                }
                else if (employee.Position == "Senior Project Enginner")
                {
                    employee.Salary = 50000;
                }
                else if (employee.Position == "Project Lead")
                {
                    employee.Salary = 60000;
                }
                else if (employee.Position == "Project Manager")
                {
                    employee.Salary = 70000;
                }
            }
            get
            {
                return employee.Position;
            }
        }
       

        /// <summary>
        /// Project in which employee is assigned
        /// </summary>
        public string ProjectName
        {
            set
            {
                employee.ProjectName = value;
            }
            get
            {
                return employee.ProjectName;
            }
        }

        /// <summary>
        /// Name of the person who employee reports to
        /// </summary>
        public string ReportsTo
        {
            set
            {
                employee.ReportsTo = value;
            }
            get
            {
                return employee.ReportsTo;
            }
        }
        static readonly string[] ValidateProperty =
        {
            "Name","PhoneNo","Address","Gender","Position","ProjectName","ReportsTo","MailId"
        };
        /// <summary>
        /// It validates all the inputs 
        /// </summary>
        /// <param name="parameter">null</param>
        /// <returns>True if all the inputs are successfully validated or else returns false</returns>
        public bool isValid(object parameter)
        {
            foreach (string property in ValidateProperty)
                if (GetValidationError(property) != null)                 
                    return false;
            return true;
        }
        /// <summary>
        /// returns the error
        /// </summary>
        public string Error
        {
            get
            {
                return string.Empty;
            }
        }
        int Check = 0;
        /// <summary>
        /// Checks and returns error message if the validation is unsuccessful or else returns null
        /// </summary>
        /// <param name="PropertyName"> The name of property which is to be validated</param>
        /// <returns>Null or error msg</returns>
        public string this[string PropertyName]
        {
            get
            {
                if (Check >= ValidateProperty.Length)
                   return GetValidationError(PropertyName);
                Check++;
                return null;
            }
        }
        string GetValidationError(string PropertyName)
        {
            string result = "";
            Regex cName = new Regex(@"^[a-zA-Z]{1}[a-zA-Z\ ]{0,}");
            Regex cPhone = new Regex(@"^[0-9]{10}$");
            Regex cEmail = new Regex(@"^[A-za-z]{1,}@solitontech\.com$");
            switch (PropertyName)
            {
                case "Name":
                    {
                        if (String.IsNullOrEmpty(Name))
                            result = "Name is Empty";
                        else if (cName.IsMatch(Name) == false)
                            result = "Name not in correct format";
                        else
                            result = null;
                        break;
                    }
                case "PhoneNo":
                    {
                        if (String.IsNullOrEmpty(PhoneNo))
                            result = "PhoneNumber is Empty";
                        else if (cPhone.IsMatch(PhoneNo) == false)
                            result = "PhoneNumber not in correct format";
                        else
                            result = null;
                        break;
                    }
                case "Address":
                    {
                        if (String.IsNullOrEmpty(Address))
                            result = "Address cannot be empty";
                        else
                            result = null;
                        break;
                    }
                case "Gender":
                    {
                        if (String.IsNullOrEmpty(Gender))
                            result = "Choose Gender";
                        else
                            result = null;
                        break;
                    }
                case "Position":
                    {
                        if (String.IsNullOrEmpty(Position))
                            result = "Choose Position";
                        else
                            result = null;
                        break;
                    }
                case "ProjectName":
                    {
                        if (String.IsNullOrEmpty(ProjectName))
                            result = "Choose ProjectName";
                        else
                            result = null;
                        break;
                    }
                case "ReportsTo":
                    {
                        if (String.IsNullOrEmpty(ReportsTo))
                            result = "Choose ReportsTo";
                        else
                            result = null;
                        break;
                    }
                case "MailId":
                    {
                        if (String.IsNullOrEmpty(MailId))
                            result = "MailID cannot be empty";
                        else if (cEmail.IsMatch(MailId) == false)
                            result = "MailID not in correct Format";
                        else
                            result = null;
                        break;
                    }
            }
            return result;
        }


       

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}