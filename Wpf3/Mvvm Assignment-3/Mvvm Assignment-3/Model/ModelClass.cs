﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows;
namespace Mvvm_Assignment_3.Model
{
    public class ModelClass
    {
        /// <summary>
        /// Observable collection which contains the details of employees
        /// </summary>
        public static ObservableCollection<ModelClassEmployeeForm> employeeList = new ObservableCollection<ModelClassEmployeeForm>();
        /// <summary>
        /// Name of employee
        /// </summary>
        public string name;
        /// <summary>
        /// JobTitle of employee
        /// </summary>
        public string jobTitle;
        /// <summary>
        /// Name of the person who the employee reports to
        /// </summary>
        public string reportingTo;
        /// <summary>
        /// Project of employee
        /// </summary>
        public string projectName;
        /// <summary>
        /// MailId of employee
        /// </summary>
        public string mailId;
        /// <summary>
        /// Phone number of employee
        /// </summary>
        public string phoneNumber;
        /// <summary>
        /// Gender of employee
        /// </summary>
        public string gender;
        /// <summary>
        /// Martial status of employee
        /// </summary>
        public string martialStatus;
        /// <summary>
        /// Image of employee
        /// </summary>
        public ImageSource employeeImage;
        /// <summary>
        /// Address of employee
        /// </summary>
        public string address;
        /// <summary>
        /// The method which is invoked by the command used to open the employeeform window
        /// </summary>
        /// <param name="parameter"></param>
        public void AddClickProp(object parameter)
        {
            EmployeeForm empForm = new EmployeeForm();
            empForm.Show();
        }
        /// <summary>
        /// The method which is invoked by the command used to delete the details of employee from listview
        /// </summary>
        /// <param name="parameter">Selected index</param>
        public void Delete_Click(object parameter)
        {

            if (MessageBox.Show("Do you want to delete the employee details?", "Confirm Deletion", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                int index = (int)parameter;
                ModelClass.employeeList.RemoveAt(index);
                name = "";
                jobTitle = "";
                reportingTo = "";
                projectName = "";
                mailId = "";
                phoneNumber = "";
                gender = "";
                martialStatus = "";
                address = "";
                employeeImage = null;


            }
        }

    }
}
