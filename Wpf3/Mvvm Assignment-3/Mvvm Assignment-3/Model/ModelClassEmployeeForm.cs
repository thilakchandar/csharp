﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows;
using Mvvm_Assignment_3.ViewModel;

namespace Mvvm_Assignment_3.Model
{
    /// <summary>
    /// Class which contains the details of employee
    /// </summary>
    public class ModelClassEmployeeForm
    {
        private string name;
        /// <summary>
        /// Name of employee
        /// </summary>
        public string Name { get { return name; } set { name = value; } }
        private string address;
        /// <summary>
        /// Employee's address
        /// </summary>
        public string Address { get { return address; } set { address = value; } }
        private ImageSource image;
        /// <summary>
        /// Image of employee
        /// </summary>
        public ImageSource Image { get { return image; } set { image = value; } }
        private string gender;
        /// <summary>
        /// Employee's gender
        /// </summary>
        public string Gender { get { return gender; } set { gender = value; } }
        private string isMarried;
        /// <summary>
        /// Employee's martial status
        /// </summary>
        public string IsMarried { get { return isMarried; } set { isMarried = value; } }
        private string phoneNo;
        /// <summary>
        /// Employee's Phone number
        /// </summary>
        public string PhoneNo { get { return phoneNo; } set { phoneNo = value; } }
        private string mailId;
        /// <summary>
        /// Mailid of employee
        /// </summary>
        public string MailId { get { return mailId; } set { mailId = value; } }
        private string position;
        /// <summary>
        /// Position of employee
        /// </summary>
        public string Position { get { return position; } set { position = value; } }
        private double salary;
        /// <summary>
        /// Salary of the employee
        /// </summary>
        public double Salary { get { return salary; } set { salary = value; } } 
        private string projectName;
        /// <summary>
        /// Project in which employee is assigned
        /// </summary>
        public string ProjectName { get { return projectName; } set { projectName = value; } }
        private string reportsTo;
        /// <summary>
        /// Name of the person who employee reports to
        /// </summary>
        public string ReportsTo { get { return reportsTo; } set { reportsTo = value; } }
        /// <summary>
        /// Method which is executed when add command is invoked
        /// </summary>
        /// <param name="parameter">Name of the window</param>
        public void AddToList(object parameter)
        {
            ViewModelClass viewModelClass = new ViewModelClass();
            viewModelClass.EmployeeList.Add(this);
            EmployeeForm form=parameter as EmployeeForm;
            form.Close();
        }
        /// <summary>
        ///  Method which is executed when close command is invoked
        /// </summary>
        /// <param name="parameter">Name of the window</param>
        public void CloseWindow(object parameter)
        {
            EmployeeForm form=parameter as EmployeeForm;
            form.Close();
        }

    }
}
