﻿using System.Windows;
using TimerApp.ViewModel;

namespace TimerApp.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new MainWindowViewModel();
            MainWindowFrame.Navigate(new LoginPage());
        }
    }
}
