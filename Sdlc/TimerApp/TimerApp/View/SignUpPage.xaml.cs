﻿using System.Windows.Controls;
using TimerApp.ViewModel;

namespace TimerApp.View
{
    /// <summary>
    /// Interaction logic for SignUpPage.xaml
    /// </summary>
    public partial class SignUpPage : Page
    {
        public SignUpPage()
        {
            InitializeComponent();
            this.DataContext = new SignUpViewModel();
        }
    }
}
