﻿using System.Windows;
using TimerApp.ViewModel;
namespace TimerApp.View
{
    /// <summary>
    /// Interaction logic for TaskName.xaml
    /// </summary>
    public partial class TaskName : Window
    {
        public TaskName()
        {
            InitializeComponent();
            this.DataContext = new TaskNameViewModel();
        }
    }
}
