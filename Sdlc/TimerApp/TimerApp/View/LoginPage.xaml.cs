﻿using System.Windows.Controls;
using TimerApp.ViewModel;

namespace TimerApp.View
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        public LoginPage()
        {
            InitializeComponent();
            this.DataContext = new LoginViewModel();
        }
    }
}
