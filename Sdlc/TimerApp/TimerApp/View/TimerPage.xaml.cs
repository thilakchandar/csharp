﻿using System.Windows.Controls;
using TimerApp.ViewModel;

namespace TimerApp.View
{
    /// <summary>
    /// Interaction logic for TimerPage.xaml
    /// </summary>
    public partial class TimerPage : Page
    {
        public TimerPage()
        {
            InitializeComponent();
            this.DataContext = new TimerViewModel(); 
        }

    }
}
