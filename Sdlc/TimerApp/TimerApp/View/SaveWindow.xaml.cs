﻿using System.Windows;
using TimerApp.ViewModel;

namespace TimerApp.View
{
    /// <summary>
    /// Interaction logic for SaveWindow.xaml
    /// </summary>
    public partial class SaveWindow : Window
    {
        public SaveWindow()
        {
            InitializeComponent();
            this.DataContext = new SaveViewModel();
        }
    }
}
