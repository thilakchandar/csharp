﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows;
using TimerApp.View;

namespace TimerApp.Model
{
    /// <summary>
    /// It is the class which represents the tasks.
    /// </summary>
    public class TaskModel : INotifyPropertyChanged
    {
        private TimelogManagementModel _model = TimelogManagementModel.GetInstance();

        private string? _name;

        private bool _taskRepeated;

        /// <summary>
        /// Observable collection of the sessions.
        /// </summary>
        private ObservableCollection<TaskSessionModel> sessions = new ();

         /// <summary>
        /// Name of the task performed.
        /// </summary>
        public string? Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Contains(','))
                {
                    value = "InvalidEdit";
                }
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        /// <summary>
        /// It is the collection of the sessions, an task as. 
        /// </summary>
        public ObservableCollection<TaskSessionModel> Sessions
        {
            get { return sessions; }
            set { sessions = value; }
        }

        /// <summary>
        /// To check if the task name is not null
        /// </summary>
        /// <param name="parameter">null</param>
        /// <returns>True if task name is not null</returns>
        public bool IfNotNullCheck(object parameter)
        {
            Regex CheckName = new Regex(@"^[a-zA-Z]{1,}[a-zA-Z0-9]{1,10}$");
            if (Name!=null && CheckName.IsMatch(Name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Used to add task in the task list which is binded to the task datagrid.
        /// </summary>
        /// <param name="parameter">instance of the window</param>
        public void AddTask(object parameter)
        {
            _taskRepeated = false;
            for (int i = 0; i < _model.TasksList.Count; i++)
            {
                if (_model.TasksList[i].Name == Name)
                {
                    _taskRepeated = true;
                }
            }
            if (_taskRepeated == true)
            {
                MessageBox.Show("Task with same name already present", "Task repeated");
            }
            else
            {
                TaskModel newTask = new TaskModel();
                TaskSessionModel taskSession = new TaskSessionModel();
                newTask.Name = Name;
                _model.TasksList.Add(newTask);
                TaskName? taskname = parameter as TaskName;
                taskname?.Close();
            }
        }

        /// <summary>
        /// Used to close the window.
        /// </summary>
        /// <param name="parameter">instance of the window</param>
        public void Close(object parameter)
        {
            TaskName? taskname = parameter as TaskName;
            taskname?.Close();
        }

        /// <summary>
        /// Event for OnPropertyChanged.
        /// </summary>
        public event PropertyChangedEventHandler? PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
