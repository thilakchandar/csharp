﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using Microsoft.Win32;
using TimerApp.View;

namespace TimerApp.Model
{
    /// <summary>
    /// This is the main class which contains the methods for the timer application.
    /// </summary>
    public class TimelogManagementModel : INotifyPropertyChanged
    {

        private static TimelogManagementModel? _mainModelInstance = TimelogManagementModel.GetInstance();

        private string? _username;

        private string? _password;

        private string? _invalid;

        private string? _confirmPassword;

        private string? _invalidMessage;

        private string _startOrStop = "Start";

        private bool _isStart = true;

        private TaskModel? _taskModel = null;

        private TaskSessionModel? _taskSessionModel = null;

        /// <summary>
        /// Folder path of the TimerSdlc folder in app data.
        /// </summary>
        public string FolderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "TimerSdlc");

        /// <summary>
        /// Observable collection of the sessions in each task
        /// </summary>
        public ObservableCollection<TaskSessionModel> TaskSession = new ObservableCollection<TaskSessionModel>();

        /// <summary>
        /// Observable collection of the tasks
        /// </summary>
        public ObservableCollection<TaskModel> TasksList = new ObservableCollection<TaskModel>();

        /// <summary>
        /// List which stores the accountDetails
        /// </summary>
        public List<string> AccountDetails = new();

        /// <summary>
        /// Username of the account
        /// </summary>
        public string? Username
        {
            get { return _username; }
            set
            {
                _username = value;
                OnPropertyChanged(nameof(Username));
            }
        }

        /// <summary>
        /// Password of the account
        /// </summary>
        public string? Password
        {
            get { return _password; }
            set 
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }

        /// <summary>
        /// Used is notify if the button should display start or stop.
        /// </summary>
        public bool IsStart
        {
            get { return _isStart; }
            set { _isStart = value; OnPropertyChanged(nameof(IsStart)); }
        }

        /// <summary>
        /// It is used to display the invalid message.
        /// </summary>
        public string? Invalid
        {
            get
            {
                return _invalid;
            }
            set
            {
                _invalid = value;
                OnPropertyChanged(nameof(Invalid));
            }
        }

        /// <summary>
        /// The value which is to be checked with the password.
        /// </summary>
        public string? ConfirmPassword
        {
            get { return _confirmPassword; }
            set
            {
                _confirmPassword = value;
                OnPropertyChanged(nameof(ConfirmPassword));
            }
        }

        /// <summary>
        /// Property to display invalid message.
        /// </summary>
        public string? InvalidMessage
        {
            get { return _invalidMessage; }
            set 
            {
                _invalidMessage = value;
                OnPropertyChanged(nameof(InvalidMessage));
            }
        }

        /// <summary>
        /// It determines whether the button should be shown as a start or stop.
        /// </summary>
        public string StartOrStop
        {
            get
            {
                return _startOrStop;
            }
            set
            {
                _startOrStop = value;
                OnPropertyChanged(nameof(StartOrStop));
            }
        }

        /// <summary>
        /// Constructor for singleton pattern
        /// </summary>
        private TimelogManagementModel() { }

        /// <summary>
        /// Used in singleton pattern to get instance.
        /// </summary>
        /// <returns>Instance of the TimelogManagement model</returns>
        public static TimelogManagementModel GetInstance()
        {
            if (_mainModelInstance == null)
            {
                _mainModelInstance = new TimelogManagementModel();
            }
            return _mainModelInstance;
        }

        /// <summary>
        /// This opens the window to add task.
        /// </summary>
        /// <param name="parameter">null</param>
        public void AddTask(object parameter)
        {
            TaskName taskName = new TaskName();
            taskName.ShowDialog();
        }

        /// <summary>
        /// Used to import files from csv files.
        /// </summary>
        /// <param name="fileName">the file which is to be imported.</param>
        /// <exception cref="FileNotFoundException">If file not found</exception>
        public void ReadFromFile(string fileName)
        {
            TasksList.Clear();
            TaskSession.Clear();
            using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                StreamReader streamReader = new StreamReader(fileStream);
                if (streamReader.BaseStream.CanSeek)
                {
                    streamReader.BaseStream.Seek(0, SeekOrigin.Begin);
                }
                streamReader.ReadLine();
                string? taskName = null;
                while (true)
                {
                    try
                    {
                        string? line = streamReader.ReadLine();
                        if (line != null)
                        {
                            string[] sessionDetails = line.Split(',');
                            if (taskName == null || taskName != sessionDetails[0])
                            {
                                if (taskName != null && _taskModel != null)
                                {
                                    TasksList.Add(_taskModel);
                                }
                                _taskModel = new TaskModel();
                                _taskModel.Name = sessionDetails[0];
                                taskName = sessionDetails[0];
                                if (sessionDetails.Length == 1)
                                {
                                    continue;
                                }
                            }
                            _taskSessionModel = new TaskSessionModel();
                            _taskSessionModel.Name = sessionDetails[1];
                            _taskSessionModel.StartDate = sessionDetails[2];
                            _taskSessionModel.StartTime = sessionDetails[3];
                            _taskSessionModel.EndTime = sessionDetails[4];
                            _taskSessionModel.Duration = TimeSpan.Parse(sessionDetails[5]);
                            if (_taskModel != null)
                            {
                                _taskModel.Sessions.Add(_taskSessionModel);
                            }
                        }
                        else
                        {
                            if (_taskModel != null && _taskModel.Sessions.Count >= 0)
                            {
                                TasksList.Add(_taskModel);
                            }
                            streamReader.Close();
                            fileStream.Close();
                            break;
                        }
                    }
                    catch
                    {
                        throw new FileNotFoundException();
                    }
                }
            }
        }

        /// <summary>
        /// Used to store data in csv files.
        /// </summary>
        /// <param name="fileName">the filepath in which the data is to be stored.</param>
        /// <exception cref="FileNotFoundException">if file not found</exception>
        public void ToStoreInFile(string fileName)
        {
            using (FileStream fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                StreamWriter streamWriter = new StreamWriter(fileStream);
                try
                {
                    streamWriter.WriteLine("TaskName,SessionNo,StartDate,StartTime,EndTime,Duration");
                    foreach (var task in TasksList)
                    {
                        string? taskName = task.Name;
                        if (task.Sessions.Count == 0)
                        {
                            streamWriter.WriteLine($"{taskName}");
                        }
                        foreach (var session in task.Sessions)
                        {
                            streamWriter.WriteLine($"{taskName},{session.Name},{session.StartDate},{session.StartTime},{session.EndTime},{session.Duration}");
                        }
                    }
                }
                catch
                {
                    throw new FileNotFoundException();
                }
                streamWriter.Close();
                fileStream.Close();
            }
        }

        /// <summary>
        /// Method which is used for closing the application.
        /// </summary>
        /// <param name="parameter">The name of the page in which this open</param>
        public void Close(object parameter)
        {
            if (parameter != null && parameter.ToString() == "TimerMainPage")
            {
                SaveWindow saveWindow = new SaveWindow();
                saveWindow.ShowDialog();
            }
            else
            {
                Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                Application.Current.Shutdown();
            }
        }

        /// <summary>
        /// Used to close the window.
        /// </summary>
        /// <param name="parameter">instance of the window</param>
        public void CloseWindow(object parameter)
        {
            SaveWindow? saveWindow = parameter as SaveWindow;
            saveWindow?.Close();
        }

        /// <summary>
        /// To reset the data present.
        /// </summary>
        /// <param name="parameter">null</param>
        public void ResetFile(object parameter)
        {
            if (MessageBox.Show("Reset permanently deletes the files, are you sure you want to reset.", "Confirm reset", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                string fileName = Path.Combine(FolderPath, $"{Username}.csv");
                TasksList.Clear();
                TaskSession.Clear();
                if (_mainModelInstance != null)
                {
                    _mainModelInstance.ToStoreInFile(fileName);
                }
            }

        }

        /// <summary>
        /// To encrypt the string passed.
        /// </summary>
        /// <param name="parameter">the string to be encrypted.</param>
        /// <returns>encrypted string</returns>
        public string Encrypt(string parameter)
        {
            string password = parameter;
            string key = "AAECAwQFBgcICQoLDA0ODwog";
            byte[] iv = new byte[16];
            byte[] array;
            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key,aes.IV);
                using (MemoryStream memoryStream=new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter writer = new StreamWriter((Stream)cryptoStream))
                        {
                            writer.Write(password);
                        }
                        array = memoryStream.ToArray();
                    }
                }
            }
            return Convert.ToBase64String(array);
        }

        /// <summary>
        /// To decrypt the string passed.
        /// </summary>
        /// <param name="parameter">the string to be decrypted</param>
        /// <returns>string which is decrypted</returns>
        public string Decrypt(string parameter)
        {
            byte[] iv = new byte[16];
            byte[] passwordEncrypted = Convert.FromBase64String(parameter.ToString());
            string key = "AAECAwQFBgcICQoLDA0ODwog";
            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream(passwordEncrypted))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// To save and close the appllication
        /// </summary>
        /// <param name="parameter">the name of the page.</param>
        public void SaveAndClose(object parameter)
        {
            _mainModelInstance.SaveFile(parameter);
            _mainModelInstance.Close(parameter);
        }

        /// <summary>
        /// To save user files
        /// </summary>
        /// <param name="parameter">null</param>
        public void SaveFile(object parameter)
        {
            string fileName = Path.Combine(FolderPath, $"{Username}.csv");
            if (_mainModelInstance != null)
            {
                _mainModelInstance.ToStoreInFile(fileName);
            }
            MessageBox.Show("Successfully saved", "Save status", MessageBoxButton.OK);
        }

        /// <summary>
        /// This is used to go to the login window.
        /// </summary>
        /// <param name="parameter">null</param>
        public void LoggingOut(object parameter)
        {
            if (MessageBox.Show("Are you sure to logout", "Confirm logout", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                string fileName = Path.Combine(FolderPath, $"{Username}.csv");
                if (_mainModelInstance != null)
                {
                    _mainModelInstance.ToStoreInFile(fileName);
                }
                Username = string.Empty;
                TasksList.Clear();
                TaskSession.Clear();
                MainWindow main = (MainWindow)Application.Current.MainWindow;
                main.MainWindowFrame.Navigate(new LoginPage());
            }
        }

        /// <summary>
        /// Method to delete task
        /// </summary>
        /// <param name="parameter">Index of the task to be deleted</param>
        public void DeleteMethod(object parameter)
        {
            int deletionIndex = (int)parameter;
            if (MessageBox.Show($"Are you sure about deleting Task: {TasksList[deletionIndex].Name}", "Confirm deletion", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                TasksList.RemoveAt(deletionIndex);
            }
        }

        /// <summary>
        /// To check if the task is selected
        /// </summary>
        /// <param name="parameter">The selected task</param>
        /// <returns>true if task is selected or else false</returns>
        public bool IfSelected(object parameter)
        {
            if (parameter == null || (int)parameter < 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// To disable the button if the timer is running and the task is not selected.
        /// </summary>
        /// <param name="parameter">Index of the selected task</param>
        /// <returns>False if timer is running or no task is selected.</returns>
        public bool RunningAndIfSelected(object parameter)
        {
            if (parameter == null || (int)parameter < 0 || !IsStart)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// To check if the timer is running
        /// </summary>
        /// <param name="parameter">null</param>
        /// <returns>False if the timer is running or else true</returns>
        public bool TimerRunning(object parameter)
        {
            if (!IsStart)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Method to export to csv files.
        /// </summary>
        /// <param name="parameter">null</param>
        public void CsvExport(object parameter)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "csv files (*.csv)|*.csv";
            if (openFileDialog.ShowDialog() == true)
            {
                if (_mainModelInstance != null)
                {
                    _mainModelInstance.ToStoreInFile(openFileDialog.FileName);
                    MessageBox.Show("Successfully exported", "Export status", MessageBoxButton.OK);
                }
            }
            else
            {
                MessageBox.Show("Export failed", "Export status", MessageBoxButton.OK);
            }
        }

        /// <summary>
        /// Method to import data from csv files.
        /// </summary>
        /// <param name="parameter">null</param>
        public void CsvImport(object parameter)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "csv files (*.csv)|*.csv";
            if (openFileDialog.ShowDialog() == true)
            {
                if (_mainModelInstance != null)
                {
                    _mainModelInstance.ReadFromFile(openFileDialog.FileName);
                    MessageBox.Show("Successfully Imported", "Import status", MessageBoxButton.OK);
                }
            }
            else
            {
                MessageBox.Show("Import failed", "Import status", MessageBoxButton.OK);
            }
        }

        /// <summary>
        /// Event of OnPropertyChanged.
        /// </summary>
        public event PropertyChangedEventHandler? PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
