﻿using System;
using System.ComponentModel;

namespace TimerApp.Model
{
    /// <summary>
    /// This class represents the details of each tasks.
    /// </summary>
    public class TaskSessionModel:INotifyPropertyChanged
    {
        private string? _name;

        private string? _startDate;

        private string? _startTime;

        private string? _endTime;

        private TimeSpan _duration;

        /// <summary>
        /// Name of the Session.
        /// </summary>
        public string? Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Contains(','))
                {
                    value = "InvalidEdit";
                }
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        /// <summary>
        /// Date in which the session is started.
        /// </summary>
        public string? StartDate
        {
            get { return _startDate; }
            set
            {
                if (value != null && value.Contains(','))
                {
                    value = "InvalidEdit";
                }
                _startDate = value;
                OnPropertyChanged(nameof(StartDate));
            }
        }

        /// <summary>
        /// Time in which the session is started.
        /// </summary>
        public string? StartTime
        {
            get { return _startTime; }
            set
            {
                if (value != null && value.Contains(','))
                {
                    value = "InvalidEdit";
                }
                _startTime = value;
                OnPropertyChanged(nameof(StartTime));
            }
        }

        /// <summary>
        /// Time in which the session is ended.
        /// </summary>
        public string? EndTime
        {
            get { return _endTime; }
            set
            {
                if (value != null && value.Contains(','))
                {
                    value = "InvalidEdit";
                }
                _endTime = value;
                OnPropertyChanged(nameof(EndTime));
            }

        }

        /// <summary>
        /// Its is the time spent doing the tast in this session.
        /// </summary>
        public TimeSpan Duration
        {
            get { return _duration; }
            set {
                _duration = value;
                OnPropertyChanged(nameof(Duration));
            }
        }

        public event PropertyChangedEventHandler? PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
