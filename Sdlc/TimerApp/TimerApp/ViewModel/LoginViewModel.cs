﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Data;
using TimerApp.Model;
using TimerApp.View;

namespace TimerApp.ViewModel
{
    /// <summary>
    /// The login page is binded with this viewmodel.
    /// </summary>
    public class LoginViewModel : INotifyPropertyChanged, IDataErrorInfo
    {
        private TimelogManagementModel _model = TimelogManagementModel.GetInstance();

        private int _check = 0;

        private bool _valid = false;

        /// <summary>
        /// Used to bind the login command.
        /// </summary>
        public RelayCommand LoginCommand { get; set; }

        /// <summary>
        /// Used to bind the signup command.
        /// </summary>
        public RelayCommand SignUpCommand { get; set; }

        /// <summary>
        /// To close the window.
        /// </summary>
        public RelayCommand CloseCommand { get; set; }

        /// <summary>
        /// Used to bind the username to the model.
        /// </summary>
        public string? Username
        {
            get
            {
                return _model.Username;
            }
            set
            {
                _model.Username = value;
            }
        }

        /// <summary>
        /// Used to bind the password to the model.
        /// </summary>
        public string? Password
        {
            get
            {
                return _model.Password;
            }
            set
            {
                _model.Password = value;
                OnPropertyChanged(nameof(Password));
            }
        }


        /// <summary>
        /// It is used to display the invalid message.
        /// </summary>
        public string? Invalid
        {
            get
            {
                return _model.Invalid;
            }
            set
            {
                _model.Invalid = value;
                OnPropertyChanged(nameof(Invalid));
            }
        }

        /// <summary>
        /// Implementation of IDataErrorInfo
        /// </summary>
        public string Error
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Constructor of the class LoginViewModel
        /// </summary>
        public LoginViewModel()
        {
            LoginCommand = new RelayCommand(Login, LoginCheck);
            SignUpCommand = new RelayCommand(SignUp);
            CloseCommand = new RelayCommand(_model.Close);
            if (!Directory.Exists(_model.FolderPath))
            {
                Directory.CreateDirectory(_model.FolderPath);
                using (FileStream fileStream = new FileStream(Path.Combine(_model.FolderPath, "Accounts.csv"), FileMode.Create, FileAccess.Write))
                {
                    StreamWriter writer = new StreamWriter(fileStream);
                    writer.WriteLine("account,password");
                    writer.Close();
                    fileStream.Close();
                }

            }
            using (FileStream fileStream = new FileStream(Path.Combine(_model.FolderPath, "Accounts.csv"), FileMode.Open, FileAccess.Read))
            {
                StreamReader streamReader = new StreamReader(fileStream);
                if (streamReader.BaseStream.CanSeek)
                {
                    streamReader.BaseStream.Seek(0, SeekOrigin.Begin);
                }
                streamReader.ReadLine();
                while (true)
                {
                    try
                    {
                        string? line = streamReader.ReadLine();
                        if (line != null)
                        {
                            string[] splitString = line.Split(',');
                            string aString;
                            for (int i = 0; i < splitString.Length; i++)
                            {
                                if (i % 2 != 0)
                                {
                                    aString = _model.Decrypt(splitString[i]);
                                }
                                else
                                {
                                    aString = splitString[i];
                                }
                                _model.AccountDetails.Add(aString);
                            }
                        }
                        else
                        {
                            streamReader.Close();
                            fileStream.Close();
                            break;
                        }
                    }
                    catch
                    {
                        throw new FileNotFoundException();
                    }
                }
            }
        }

        /// <summary>
        /// Used to navigate to the timer page
        /// </summary>
        /// <param name="parameter">null</param>
        public void Login(object parameter)
        {
            for (int i = 0; i < _model.AccountDetails.Count; i += 2)
            {
                if (_model.AccountDetails[i] == Username && _model.AccountDetails[i + 1] == Password)
                {
                    _valid = true;
                    Invalid = string.Empty;
                    string fileName = Path.Combine(_model.FolderPath, $"{Username}.csv");
                    _model.ReadFromFile(fileName);
                    MainWindow main = (MainWindow)Application.Current.MainWindow;
                    main.MainWindowFrame.Navigate(new TimerPage());
                    Password = string.Empty;
                }
                if (_valid == false)
                {
                    Invalid = "Invalid username or password";
                }
            }
        }


        /// <summary>
        /// Used to check if the login button can be enabled.
        /// </summary>
        /// <param name="parameter">null</param>
        /// <returns>True if the values entered in the username and password is valid or else false </returns>
        public bool LoginCheck(object parameter)
        {
            if (GetValidationError("Username") == null && GetValidationError("Password") == null && Directory.Exists(_model.FolderPath))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Used to navigate to then signup page
        /// </summary>
        /// <param name="parameter">null</param>
        public void SignUp(object parameter)
        {
            Invalid = string.Empty;
            MainWindow main = (MainWindow)Application.Current.MainWindow;
            main.MainWindowFrame.Navigate(new SignUpPage());
        }

        /// <summary>
        /// Used for validating the properties
        /// </summary>
        /// <param name="propertyName">The properties of this class</param>
        /// <returns>Error message</returns>
        public string? this[string propertyName]
        {
            get
            {
                if (_check >= 2)
                {
                    return GetValidationError(propertyName);
                }
                _check++;
                return null;
            }
        }

        /// <summary>
        /// Event used for Onproperty changed
        /// </summary>
        public event PropertyChangedEventHandler? PropertyChanged;

        private string? GetValidationError(string propertyName)
        {
            string? result = string.Empty;
            Regex checkUsername = new Regex(@"^[a-zA-Z]{1,20}$");
            Regex checkPassword = new Regex(@"^[A-Za-z0-9!@#$&%*()\\\-`.+,]{1,20}$");
            switch (propertyName)
            {
                case "Username":
                    {
                        if (string.IsNullOrEmpty(Username))
                        {
                            result = "Username is Empty";
                        }
                        else if (checkUsername.IsMatch(Username) == false)
                        {
                            result = "Username not in correct format";
                        }
                        else
                        {
                            result = null;
                        }
                        break;
                    }
                case "Password":
                    {
                        if (string.IsNullOrEmpty(Password))
                        {
                            result = "Password is Empty";
                        }
                        else if (checkPassword.IsMatch(Password) == false)
                        {
                            result = "Password not in correct format";
                        }
                        else
                        {
                            result = null;
                        }
                        break;
                    }

            }
            return result;
        }

        /// <summary>
        /// Method used for Onproperty changed.
        /// </summary>
        /// <param name="propertyName">name of the property which is to be changed</param>
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

}
