﻿using TimerApp.Model;
namespace TimerApp.ViewModel
{
    /// <summary>
    /// The TaskNameWindow is binded to this viewmodel.
    /// </summary>
    public class TaskNameViewModel
    {
        TaskModel taskModel = new();

        /// <summary>
        /// Command used to update if the task name.
        /// </summary>
        public RelayCommand Ok { get; set; }

        /// <summary>
        /// Command used to close the task name window without updating task name.
        /// </summary>
        public RelayCommand Cancel { get; set; }

        /// <summary>
        /// Used to bind the task name.
        /// </summary>
        public string? TaskName { get { return taskModel.Name; } set { taskModel.Name = value; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public TaskNameViewModel()
        {
            Ok = new RelayCommand(taskModel.AddTask, taskModel.IfNotNullCheck);
            Cancel = new RelayCommand(taskModel.Close);
        }

    }
}
