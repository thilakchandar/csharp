﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace TimerApp.ViewModel.Converters
{
    /// <summary>
    /// Class to implement password converters.
    /// </summary>
    public class PasswordConverter : IValueConverter
    {
        /// <summary>
        /// Used to convert the value to the desired one.
        /// </summary>
        /// <param name="value">the value to be converted</param>
        /// <param name="targetType">the return type</param>
        /// <param name="parameter">null</param>
        /// <param name="culture">null</param>
        /// <returns>the * value</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new string('*', value?.ToString().Length ?? 0);
        }

        /// <summary>
        /// Used to convert back the value. Not implemented here.
        /// </summary>
        /// <param name="value">null</param>
        /// <param name="targetType">null</param>
        /// <param name="parameter">null</param>
        /// <param name="culture">null</param>
        /// <returns>null</returns>
        /// <exception cref="NotImplementedException"></exception>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
