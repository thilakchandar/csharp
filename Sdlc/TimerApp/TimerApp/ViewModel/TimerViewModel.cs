﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Timers;
using TimerApp.Model;

namespace TimerApp.ViewModel
{
    /// <summary>
    /// The TimePage is binded with this viewmodel.
    /// </summary>
    public class TimerViewModel : INotifyPropertyChanged
    {
        private TimelogManagementModel _model = TimelogManagementModel.GetInstance();

        TimeSpan _time;

        private int _index;

        private Timer timer;

        private TaskSessionModel _sessionModel = null;

        /// <summary>
        /// Used is notify if the button should display start or stop.
        /// </summary>
        public bool IsStart
        {
            get { return _model.IsStart; }
            set { _model.IsStart = value; }
        }

        /// <summary>
        /// Its is used to determine if the button should be displayed as start or stop.
        /// </summary>
        public string StartOrStop
        {
            get
            {
                return _model.StartOrStop;
            }
            set
            {
                _model.StartOrStop = value;
                OnPropertyChanged(nameof(StartOrStop));
            }
        }

        /// <summary>
        /// List of the class TasksModel containing task name and list of sessions.
        /// </summary>
        public ObservableCollection<TaskModel> TasksList
        {
            get
            {
                return _model.TasksList;
            }
            set
            {
                _model.TasksList = value;
            }
        }

        /// <summary>
        /// List containing session details
        /// </summary>
        public ObservableCollection<TaskSessionModel> SessionsList
        {
            get
            {
                if (_model.TasksList.Count > 0)
                {
                    return _model.TasksList[Index].Sessions;
                }
                return null;
            }
            set
            {
                _model.TasksList[Index].Sessions = value;
                OnPropertyChanged(nameof(SessionsList));
            }
        }

        /// <summary>
        /// Index of the task which is selected.
        /// </summary>
        public int Index
        {
            get { return _index; }
            set
            {
                _index = value;
                OnPropertyChanged(nameof(Index));
            }
        }

        /// <summary>
        /// Command to open new task.
        /// </summary>
        public RelayCommand New { get; set; }

        /// <summary>
        /// Command to delete task.
        /// </summary>
        public RelayCommand Delete { get; set; }

        /// <summary>
        /// Command to start or stop time.
        /// </summary>
        public RelayCommand StartStop { get; set; }

        /// <summary>
        /// Command to logout.
        /// </summary>
        public RelayCommand Logout { get; set; }

        /// <summary>
        /// To import csv files.
        /// </summary>
        public RelayCommand Import { get; set; }

        /// <summary>
        /// To export csv files.
        /// </summary>
        public RelayCommand Export { get; set; }

        /// <summary>
        /// To save user details.
        /// </summary>
        public RelayCommand Save { get; set; }

        /// <summary>
        /// To reset user details.
        /// </summary>
        public RelayCommand Reset { get; set; }

        /// <summary>
        /// To close the window.
        /// </summary>
        public RelayCommand Close { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public TimerViewModel()
        {
            New = new RelayCommand(_model.AddTask, _model.TimerRunning);
            Delete = new RelayCommand(_model.DeleteMethod, _model.RunningAndIfSelected);
            StartStop = new RelayCommand(StartAndStop, _model.IfSelected);
            Logout = new RelayCommand(_model.LoggingOut, _model.TimerRunning);
            Import = new RelayCommand(_model.CsvImport, _model.TimerRunning);
            Export = new RelayCommand(_model.CsvExport, _model.TimerRunning);
            Save = new RelayCommand(_model.SaveFile, _model.TimerRunning);
            Reset = new RelayCommand(_model.ResetFile, _model.TimerRunning);
            Close = new RelayCommand(_model.Close, _model.TimerRunning);
        }

        /// <summary>
        /// Method to start and stop time.
        /// </summary>
        /// <param name="parameter">Index of the task</param>
        public void StartAndStop(object parameter)
        {
            Index = (int)parameter;
            if (IsStart == true)
            {
                StartOrStop = "Stop";
                IsStart = false;
                _time = new TimeSpan(0, 0, 0);
                int sessionNo = _model.TasksList[Index].Sessions.Count + 1;
                _sessionModel = new TaskSessionModel();
                _sessionModel.Name = "Session" + sessionNo;
                _sessionModel.StartDate = DateTime.Now.ToString("dd/MM/yy");
                _sessionModel.StartTime = DateTime.Now.ToString("hh:mm:ss");
                timer = new Timer();
                timer.Interval = 1000;
                timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                timer.Start();
                SessionsList.Add(_sessionModel);
            }
            else
            {
                StartOrStop = "Start";
                IsStart = true;
                timer.Stop();
                _sessionModel.EndTime = DateTime.Now.ToString("hh:mm:ss");
            }
            OnPropertyChanged(nameof(SessionsList));
        }

        /// <summary>
        /// Event of OnPropertyChanged.
        /// </summary>
        public event PropertyChangedEventHandler? PropertyChanged;

        /// <summary>
        /// Private property for OnpropertyChanged.
        /// </summary>
        /// <param name="propertyName">The name of the property which is to be changed.</param>
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            TimeSpan oneSecond = new TimeSpan(0, 0, 1);
            _time = _time.Add(oneSecond);
            _sessionModel.Duration = _time;
        }
    }
}
