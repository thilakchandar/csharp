﻿using System.ComponentModel;

namespace TimerApp.ViewModel
{
    /// <summary>
    /// This viewmodel is binded to the MainWindow
    /// </summary>
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private string _backGround = "White";

        private bool _isDark;

        /// <summary>
        /// It denotes the background of the window.
        /// </summary>
        public string BackGround
        {
            get
            {
                return _backGround;
            }
            set
            {
                _backGround = value;
                OnPropertyChanged(nameof(BackGround));
            }
        }

        /// <summary>
        /// This denotes if the toggle button is enabled or not.
        /// </summary>
        public bool IsDark
        {
            get
            {
                return _isDark;
            }
            set
            {
                _isDark = value;
                if (_isDark)
                {
                    BackGround = "Gray";
                }
                else
                {
                    BackGround = "White";
                }
            }
        }

        /// <summary>
        /// This is the event for OnProperty changed.
        /// </summary>
        public event PropertyChangedEventHandler? PropertyChanged;

        /// <summary>
        /// Method used for Onproperty changed.
        /// </summary>
        /// <param name="propertyName">name of the property which is to be changed</param>
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
