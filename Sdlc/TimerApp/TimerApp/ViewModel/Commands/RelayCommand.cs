﻿using System;
using System.Windows.Input;

namespace TimerApp.ViewModel
{
    /// <summary>
    /// Class with Icommand interface
    /// </summary>
    public class RelayCommand : ICommand
    {
        private Action<object> _execute;

        private Predicate<object> _canExecute;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="execute">The method to be executed once this command is invoked</param>
        /// <param name="canExecute">Tells if this command is executable </param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        /// <summary>
        /// Another constructor with one parameter
        /// </summary>
        /// <param name="execute">The method to be executed once this command is invoked</param>
        public RelayCommand(Action<object> execute) : this(execute, null)
        {

        }

        /// <summary>
        /// Event handler of command class
        /// </summary>
        public event EventHandler? CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Tells if this command is executable
        /// </summary>
        /// <param name="parameter">vary with command</param>
        /// <returns>true if the command can be executed or else false</returns>
        public bool CanExecute(object parameter)
        {
            return this._canExecute == null || this._canExecute(parameter);
        }

        /// <summary>
        /// he method to be executed once this command is invoked
        /// </summary>
        /// <param name="parameter">vary with command</param>
        public void Execute(object parameter)
        {
            if (_execute != null)
            {
                _execute(parameter);
            }
        }
    }
}
