﻿using TimerApp.Model;

namespace TimerApp.ViewModel
{
    /// <summary>
    /// The SaveWindow is binded with this viewmodel.
    /// </summary>
    public class SaveViewModel
    {
        private TimelogManagementModel _model = TimelogManagementModel.GetInstance();

        /// <summary>
        /// Command used to save the user details in the files.
        /// </summary>
        public RelayCommand SaveCommand { get; set; }

        /// <summary>
        /// Command which closes the application without saving.
        /// </summary>
        public RelayCommand DontSaveCommand { get; set; }

        /// <summary>
        /// Command which used to close this window.
        /// </summary>
        public RelayCommand CancelCommand { get; set; }

        public SaveViewModel()
        {
            SaveCommand = new RelayCommand(_model.SaveAndClose);
            DontSaveCommand = new RelayCommand(_model.Close);
            CancelCommand = new RelayCommand(_model.CloseWindow);
        }

    }
}
