﻿using System;
using System.ComponentModel;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using TimerApp.Model;
using TimerApp.View;

namespace TimerApp.ViewModel
{
    /// <summary>
    /// The SignUpPage is binded to this viewmodel.
    /// </summary>
    public class SignUpViewModel : INotifyPropertyChanged, IDataErrorInfo
    {
        private TimelogManagementModel _mainModel = TimelogManagementModel.GetInstance();

        private int _check = 0;

        private string _invalidMessage;

        private bool _isValid = true;

        private string? _confirmPassword;

        /// <summary>
        /// Command to go to the TimerPage binded to signup and login button
        /// </summary>
        public RelayCommand ToTimer
        {
            get;
            set;
        }

        /// <summary>
        /// To close the window.
        /// </summary>
        public RelayCommand CloseCommand { get; set; }

        /// <summary>
        /// Username to be added to the account
        /// </summary>
        public string? Username
        {
            get
            {
                return _mainModel.Username;
            }
            set
            {
                _mainModel.Username = value;
            }
        }

        /// <summary>
        /// Password that is used to confirm if the intended password is correctly updated.
        /// </summary>
        public string ConfirmPassword
        {
            get
            {
                return _confirmPassword;
            }
            set
            {
                _confirmPassword = value;
                OnPropertyChanged(nameof(ConfirmPassword));
            }
        }

        /// <summary>
        /// Password which is to be updated to the account.
        /// </summary>
        public string? Password
        {
            get
            {
                return _mainModel.Password;
            }
            set
            {
                _mainModel.Password = value;
                OnPropertyChanged(nameof(Password));
            }
        }

        /// <summary>
        /// Implementation of IDataErrorInfo
        /// </summary>
        public string Error
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// It is used to show the invalid message
        /// </summary>
        public string InvalidMessage
        {
            get
            {
                return _invalidMessage;
            }
            set
            {
                _invalidMessage = value;
                OnPropertyChanged(nameof(InvalidMessage));
            }
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public SignUpViewModel()
        {
            ToTimer = new RelayCommand(LoginSignUp, CheckDetails);
            CloseCommand = new RelayCommand(_mainModel.Close);
        }

        /// <summary>
        /// Method which is used to update the csv file in the app data.
        /// </summary>
        /// <param name="parameter">null</param>
        public void LoginSignUp(object parameter)
        {
            for (int i = 0; i < _mainModel.AccountDetails.Count; i += 2)
            {
                if (_mainModel.AccountDetails[i] == Username)
                {
                    _isValid = false;
                    break;
                }
            }
            if (_isValid == true)
            {
                using (FileStream fileStream = new FileStream(Path.Combine(_mainModel.FolderPath, "Accounts.csv"), FileMode.Append, FileAccess.Write))
                {
                    StreamWriter streamWriter = new StreamWriter(fileStream);
                    try
                    {
                        streamWriter.WriteLine($"{Username},{_mainModel.Encrypt(Password)}");
                        streamWriter.Close();
                        fileStream.Close();
                    }
                    catch
                    {
                        throw new FileNotFoundException();
                    }
                }
                string newFileName = Path.Combine(_mainModel.FolderPath, $"{Username}.csv");
                using (FileStream fileStream = File.Create(newFileName)) ;
                Password = string.Empty;
                ConfirmPassword = string.Empty;
                MainWindow main = (MainWindow)Application.Current.MainWindow;
                main.MainWindowFrame.Navigate(new TimerPage());
            }
            else
            {
                InvalidMessage = "Username already exists";
            }
        }

        /// <summary>
        /// To check if the details provided is valid.
        /// </summary>
        /// <param name="parameter">null</param>
        /// <returns>True if the details are given correctly or else false</returns>
        public bool CheckDetails(object parameter)
        {
            if (GetValidationError("Username") == null && GetValidationError("Password") == null && Password == ConfirmPassword)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Used for validating the properties
        /// </summary>
        /// <param name="propertyName">The properties of this class</param>
        /// <returns>Error message</returns>
        public string? this[string propertyName]
        {
            get
            {
                if (_check >= 3)
                {
                    return GetValidationError(propertyName);
                }
                _check++;
                return null;
            }
        }

        /// <summary>
        /// Event of INotifyPropertyChanged.
        /// </summary>
        public event PropertyChangedEventHandler? PropertyChanged;

        private string? GetValidationError(string propertyName)
        {
            string result = string.Empty;
            Regex checkUsername = new Regex(@"^[a-zA-Z]{1,20}$");
            Regex checkPassword = new Regex(@"^[A-Za-z0-9!@#$&%*()\\\-`.+]{1,20}$");
            switch (propertyName)
            {
                case "Username":
                    {
                        if (string.IsNullOrEmpty(Username))
                        {
                            result = "Username is Empty";
                        }
                        else if (checkUsername.IsMatch(Username) == false)
                        {
                            result = "Username not in correct format";
                        }
                        else
                        {
                            result = null;
                        }
                        break;
                    }
                case "Password":
                    {
                        if (string.IsNullOrEmpty(Password))
                        {
                            result = "Password is Empty";
                        }
                        else if (checkPassword.IsMatch(Password) == false)
                        {
                            result = "Password not in correct format";
                        }
                        else
                        {
                            result = null;
                        }
                        break;
                    }
                case "ConfirmPassword":
                    {
                        if (string.IsNullOrEmpty(Password))
                        {
                            result = "Password is Empty";
                        }
                        else if (checkPassword.IsMatch(Password) == false)
                        {
                            result = "Password not in correct format";
                        }
                        else
                        {
                            result = null;
                        }
                        break;
                    }

            }
            return result;
        }

        /// <summary>
        /// Method used for Onproperty changed.
        /// </summary>
        /// <param name="propertyName">name of the property which is to be changed</param>
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
