﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.Model.TimelogManagementModel")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.Model.TaskModel")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.ViewModel.LoginViewModel")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.ViewModel.SignUpViewModel")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.ViewModel.Converters.PasswordConverter")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.ViewModel.PasswordConverter")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.ViewModel.TextToPasswordCharConverter")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.Model.TaskSessionModel")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.ViewModel.MainWindowViewModel")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.ViewModel.SaveViewModel")]
[assembly: SuppressMessage("Style", "NI1704:Identifiers should be spelled correctly", Justification = "<Pending>", Scope = "member", Target = "~M:TimerApp.Model.TimelogManagementModel.Encrypt(System.Object)~System.String")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.View.SaveWindow")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.View.SignUpPage")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.ViewModel.RelayCommand")]
[assembly: SuppressMessage("Style", "NI1704:Identifiers should be spelled correctly", Justification = "<Pending>", Scope = "member", Target = "~P:TimerApp.ViewModel.SaveViewModel.DontSaveCommand")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.ViewModel.TaskNameViewModel")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.ViewModel.TimerViewModel")]
[assembly: SuppressMessage("Style", "NI1704:Identifiers should be spelled correctly", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.Model.TimelogManagementModel")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.ViewModel.Converters")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.View.MainWindow")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.View.TimerPage")]
[assembly: SuppressMessage("Correctness", "LRT001:There is only one restricted namespace", Justification = "<Pending>", Scope = "type", Target = "~T:TimerApp.View.LoginPage")]
