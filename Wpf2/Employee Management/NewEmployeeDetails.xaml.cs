﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace Employee_Management
{
    /// <summary>
    /// Interaction logic for NewEmployeeDetails.xaml
    /// </summary>
    public partial class NewEmployeeDetails : Window
    {
        public NewEmployeeDetails()
        {
            InitializeComponent();
        }
        bool validName = false;
        bool ValidAddress = false;
        bool validPhoneNo = false;
        bool validMailId = false;
        bool validReportTo = false;

        private void Name_SelectionChanged(object sender, RoutedEventArgs e)
        {
            string name = this.Name.Text;           
            Regex regex = new Regex(@"^[a-zA-Z]{1}[a-zA-Z\ ]{0,}$");
            Match match = regex.Match(name);
            if (match.Success)
            {              
                this.Name.BorderBrush = Brushes.White;
                this.IncorrectName.Text = "";
                validName = true;
            }
            else
            {
                this.Name.BorderBrush = Brushes.Red;
                this.IncorrectName.Text = "!InValid";
            }
        }

        private void Adress_SelectionChanged(object sender, RoutedEventArgs e)
        {
            string address = this.Address.Text;
           
            if (string.IsNullOrEmpty(address))
            {
                this.Address.BorderBrush = Brushes.Red;
                this.IncorrectAddress.Text = "!InValid";
            }
            else
            {
                this.Address.BorderBrush = Brushes.White;
                this.IncorrectAddress.Text = "";
                ValidAddress = true;
            }
        }

        private void PhotoButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                Uri uri = new Uri(openFileDialog.FileName);
                Image.Source= new BitmapImage(uri);
            }
        }

        private void PhoneNo_SelectionChanged(object sender, RoutedEventArgs e)
        {
            string phoneNo = this.PhoneNo.Text;
            Regex regex = new Regex(@"^[0-9]{10}$");
            Match match = regex.Match(phoneNo);
            if (match.Success)
            {
                this.PhoneNo.BorderBrush = Brushes.White;
                this.IncorrectPhoneNo.Text = "";
                validPhoneNo = true;
            }
            else
            {
                this.PhoneNo.BorderBrush = Brushes.Red;
                this.IncorrectPhoneNo.Text = "!InValid";
            }
        }

        private void MailId_SelectionChanged(object sender, RoutedEventArgs e)
        {
            string mailId = this.MailId.Text;
            Regex regex = new Regex(@"@solitontech\.com$");
            Match match = regex.Match(mailId);
            if (match.Success)
            {
                this.MailId.BorderBrush = Brushes.White;
                this.IncorrectMailId.Text = "";
                validMailId = true;
            }
            else
            {
                this.MailId.BorderBrush = Brushes.Red;
                this.IncorrectMailId.Text = "!InValid";
            }
        }

        private void ReportTo_SelectionChanged(object sender, RoutedEventArgs e)
        {
            string reportTo = this.ReportTo.Text;
            Regex regex = new Regex(@"^[a-zA-Z]{1}[a-zA-Z\ ]{0,}$");
            Match match = regex.Match(reportTo);
            if (match.Success)
            {
                this.ReportTo.BorderBrush = Brushes.White;
                this.IncorrectReportTo.Text = "";
                validReportTo = true;               
            }
            else
            {
                this.ReportTo.BorderBrush = Brushes.Red;
                this.IncorrectReportTo.Text = "!InValid";
            }
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            if (validName && ValidAddress && validPhoneNo && validMailId && validReportTo && (Position.SelectedValue != null) && (Project.SelectedValue != null) && (Male.IsChecked !=false || Female.IsChecked !=false) && (Married.IsChecked != false || NotMarried.IsChecked != false) && (Married.IsChecked!=true||NotMarried.IsChecked!=true))
            {
                Employee employee = new Employee();
                employee.Name = Name.Text;
                employee.Address = Address.Text;
                employee.Image = Image.Source;
                if (Male.IsChecked != false)
                {
                    employee.Gender = "Male";
                }
                else
                {
                    employee.Gender = "Female";
                }
                if (Married.IsChecked != false)
                {
                    employee.IsMarried = "Married";
                }
                else
                {
                    employee.IsMarried = "Single";
                }
                employee.PhoneNo = PhoneNo.Text;
                employee.MailId = MailId.Text;
                ComboBoxItem position = (ComboBoxItem)Position.SelectedValue;
                employee.Position = (string)position.Content;
                ComboBoxItem project = (ComboBoxItem)Project.SelectedValue;
                employee.ProjectName = (string)project.Content;
                employee.ReportsTo = ReportTo.Text;
                MainWindow.employeeList.Add(employee);
                MainWindow.instance.EmployeeListView.Items.Add(employee);
                
                Close();
            }
            else
            {
                Validation.Text = "Check if the values entered are valid";    
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWin = new MainWindow();
            Close();
            mainWin.Show();
        }
     
    }
}
