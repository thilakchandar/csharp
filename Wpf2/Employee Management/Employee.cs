﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Employee_Management
{
    /// <summary>
    /// Class which contains the details of employee
    /// </summary>
    public class Employee
    {
        private string name;
        /// <summary>
        /// Name of employee
        /// </summary>
        public string Name { get { return name; } set { name = value; } }
        private string address;
        /// <summary>
        /// Employee's address
        /// </summary>
        public string Address { get { return address; } set { address = value; } }
        private ImageSource image;
        /// <summary>
        /// Image of employee
        /// </summary>
        public ImageSource Image { get { return image; } set { image = value; } }
        private string gender;
        /// <summary>
        /// Employee's gender
        /// </summary>
        public string Gender { get { return gender; } set { gender = value; }}
        private string isMarried;
        /// <summary>
        /// Employee's martial status
        /// </summary>
        public string IsMarried { get { return isMarried; } set { isMarried = value; }}
        private string phoneNo;
        /// <summary>
        /// Employee's Phone number
        /// </summary>
        public string PhoneNo { get { return phoneNo; } set { phoneNo = value; }}   
        private string mailId;
        /// <summary>
        /// Mailid of employee
        /// </summary>
        public string MailId { get { return mailId; } set { mailId = value; }}
        private string position;
        /// <summary>
        /// Position of employee
        /// </summary>
        public string Position { get { return position; } set { position = value; }}
        private string projectName;
        /// <summary>
        /// Project in which employee is assigned
        /// </summary>
        public string ProjectName { get { return projectName; } set { projectName = value; }}
        private string reportsTo;
        /// <summary>
        /// Name of the person who employee reports to
        /// </summary>
        public string ReportsTo { get { return reportsTo; } set { reportsTo = value; }}


    }
}
