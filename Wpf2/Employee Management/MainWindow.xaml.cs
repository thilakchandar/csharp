﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using Microsoft.Win32;

namespace Employee_Management
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        /// <summary>
        /// List which contains the employee details
        /// </summary>
        public static List<Employee> employeeList = new List<Employee>();
        /// <summary>
        /// Contains index of the listview
        /// </summary>
        public int index;
        /// <summary>
        /// Instance of mainwindow
        /// </summary>
        public static MainWindow instance;
        private void MainAdd_Click(object sender, RoutedEventArgs e)
        {
            NewEmployeeDetails empDetails = new NewEmployeeDetails();
            empDetails.Show();
            instance = this;
            //this.Close();
        }
        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Do you want to delete the employee details?", "Confirm Deletion", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                index = EmployeeListView.SelectedIndex;
                EmployeeListView.Items.RemoveAt(index);
                employeeList.RemoveAt(index);
                NAME.Text = "";
                JOBTITLE.Text = "";
                REPORTINGTO.Text = "";
                PROJECTNAME.Text = "";
                MAILID.Text = "";
                PHONENUMBER.Text = "";
                GENDER.Text = "";
                MARITALSTATUS.Text = "";
                ADDRESS.Text = "";
                EmployeeImage.Source = null;


            }
        }

        private void EmployeeListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            index = EmployeeListView.SelectedIndex;
            NAME.Text = employeeList[index].Name;
            JOBTITLE.Text = employeeList[index].Position;
            REPORTINGTO.Text = employeeList[index].ReportsTo;
            PROJECTNAME.Text = employeeList[index].ProjectName;
            MAILID.Text = employeeList[index].MailId;
            PHONENUMBER.Text = employeeList[index].PhoneNo;
            GENDER.Text = employeeList[index].Gender;
            MARITALSTATUS.Text = employeeList[index].IsMarried;
            ADDRESS.Text = employeeList[index].Address;
            EmployeeImage.Source = employeeList[index].Image;
        }
    }
   
}
