﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfAssignment4.ViewModel;
namespace WpfAssignment4
{
    /// <summary>
    /// Interaction logic for EmployeeForm.xaml
    /// </summary>
    public partial class EmployeeForm : Window
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public EmployeeForm()
        {
            InitializeComponent();
            ViewModelClassEmployeeForm vm2 = new ViewModelClassEmployeeForm();
            DataContext = vm2;
        }
    }
}