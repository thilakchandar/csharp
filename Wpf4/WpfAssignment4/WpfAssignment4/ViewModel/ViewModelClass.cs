﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfAssignment4.Model;
using System.Windows.Media;
using System.Windows;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace WpfAssignment4.ViewModel
{
    public class ViewModelClass
    {
        /// <summary>
        /// Property to bind observableCollection which contains details of employee
        /// </summary>
        public ObservableCollection<EmployeeFormModelClass> EmployeeList
        {
            get { return ModelClass.employeeList; }
            set { ModelClass.employeeList = value; }
        }
        ModelClass mainModel = new ModelClass();
        /// <summary>
        /// Command which is invoked when add button is clicked
        /// </summary>
        public RelayCommand Add_Button_Click { get; set; }
        /// <summary>
        /// Command which is invoked when delete button is clicked
        /// </summary>
        public RelayCommand Delete_Button_Click { get; set; }
        /// <summary>
        /// Constructor 
        /// </summary>
        public ViewModelClass()
        {
            Add_Button_Click = new RelayCommand(mainModel.AddClickProp);
            Delete_Button_Click = new RelayCommand(mainModel.Delete_Click, mainModel.IsValueSelected);
        }
        /// <summary>
        /// Name of employee
        /// </summary>
        public string Name { get { return mainModel.name; } set { mainModel.name = value; } }
        /// <summary>
        /// Job title of employee
        /// </summary>
        public string JobTitle { get { return mainModel.jobTitle; } set { mainModel.jobTitle = value; } }
        /// <summary>
        /// The name of the person who the employee reports to
        /// </summary>
        public string ReportingTo { get { return mainModel.reportingTo; } set { mainModel.reportingTo = value; } }
        /// <summary>
        /// Project in which employee is working on
        /// </summary>
        public string ProjectName { get { return mainModel.projectName; } set { mainModel.projectName = value; } }
        /// <summary>
        /// Mail id of employee
        /// </summary>
        public string MailId { get { return mainModel.mailId; } set { mainModel.mailId = value; } }
        /// <summary>
        /// Phone number of employee
        /// </summary>
        public string PhoneNumber { get { return mainModel.phoneNumber; } set { mainModel.phoneNumber = value; } }
        /// <summary>
        /// Gender of employee
        /// </summary>
        public string Gender { get { return mainModel.gender; } set { mainModel.gender = value; } }
        /// <summary>
        /// Martial status of employee
        /// </summary>
        public string MartialStatus { get { return mainModel.martialStatus; } set { mainModel.martialStatus = value; } }
        /// <summary>
        /// Image of employee
        /// </summary>
        public ImageSource EmployeeImage { get { return mainModel.employeeImage; } set { mainModel.employeeImage = value; } }
        /// <summary>
        /// Address of employee
        /// </summary>
        public string Address { get { return mainModel.address; } set { mainModel.address = value; } }

    }


}
